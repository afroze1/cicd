@project_creation
Feature: Test project creation in Wamly application
  Verify that the functionalities in Wamly application is working correctly

  Background: Verify login
    Given User launches the application url
    When User Login to the wamly application with valid credentials
    Then User should see the home page title


  @projectCreation
  Scenario: Verify that candidate is able to attend the interview after successfully completing the skill test
    Given User checks the forms options
    Then User checks the forms labels
    Then User creates a form
    And User creates the skill test questions
    And User creates the skill test
#    And User creates the project
#    And User adds the form into the project
#    And User activates the project