@loginTest
Feature: Test Login page in Wamly application
  Verify that the login functionality in Wamly application is working correctly

  Background: Wamly launch
    Given User launches the application url

#  @loginPage
#  Scenario Outline: Verify that the Wamly Login page is working correctly
#    Given User attempts login using "<username>" and "<password>"
#    Examples:
#      | username     | password |
#      | blank        | valid    |
#      | valid        | blank    |
#      | unregistered | valid    |
#      | invalid      | blank    |
#      | valid        | invalid  |
#      | blank        | blank    |
#      | valid        | valid    |
#
#
#  @loginPage
#  Scenario: Verify that the SHOW-HIDE button is working correctly
#    Then User should be able to interact with the SHOW-HIDE button


  @loginPage
  Scenario: Verify login
    Given User Login to the wamly application with valid credentials
    Then User should see the home page title
#    And User creates the Interview Guides
# And User checks the Interview Guide options


    @Users
    Scenario: Create Users
      Given User Login to the wamly application with valid credentials
      Then User should see the home page title
      And User creates a new user
#     And User fetches confirmation email

    
