@candidate_journey
Feature: Test candidate journey in Wamly application
  Verify that the functionalities in Wamly application is working correctly

  Background: Verify login
    Given User Login to the application with valid credentials
    Then User should be logged in successfully

  @WM-1
  Scenario: Verify that candidate is able to attend the interview after successfully completing the skill test
    Given User created a form through form api
    And User calls the skill test questions api
    And User calls the skill test api
    And User calls the project creation api
    And User calls the api for adding a form into the project
    And User calls project activation api
    And Candidate launches the interview link
    Then Candidate should see the explainer video
    And Candidate navigates through the welcome page with valid credentials
    Then Candidate Validates the Project name on the Dashboard
    And Candidate fetches the OTP from email
    And Candidate enters a valid OTP and clicks Next
    Then Candidate should see the Intro video and proceed
    And Candidate navigates through the personal information page
    Then Candidate should see the Skills Test Info Page
    And Candidate attends the skill test successfully
    Then Candidate validates all skill test questions
    And Candidate proceeds to the pre interview checks
    And Candidate navigates through the browser check
    And Candidate navigates through the camera and microphone check
    And Candidate goes into the practice test and interface tour
    Then Candidate starts the interview
    And Candidate attends the interview
    Then Candidate receives the Interview completed email
    And Candidate rates the experience
    And Candidate goes back to the dashboard login page
    And Candidate navigates through the welcome page with valid credentials
    And Candidate fetches the OTP from email
    And Candidate enters a valid OTP and clicks Next
    And Candidate Login to the dashboard
    And Candidate Views the Project Name on the Dashboard
    Then Candidate Views the interview status as Complete

  @WM-2
  Scenario: Verify that candidate is not able to attend the interview after failing the skill test
    Given User created a form through form api
    And User calls the skill test questions api
    And User calls the skill test api
    And User calls the project creation api
    And User calls the api for adding a form into the project
    And User calls project activation api
    And Candidate launches the interview link
    Then Candidate should see the explainer video
    And Candidate navigates through the welcome page with valid credentials
    Then Candidate Validates the Project name on the Dashboard
    And Candidate fetches the OTP from email
    And Candidate enters a valid OTP and clicks Next
    Then Candidate should see the Intro video and proceed
    And Candidate navigates through the personal information page
    Then Candidate should see the Skills Test Info Page
    And Candidate attends the skill test and fails
    Then Candidate validates all skill test questions
    Then Candidate receives the Interview completed email
    And Candidate navigates back to the Dashboard
    And Candidate navigates through the welcome page with valid credentials
    And Candidate fetches the OTP from email
    And Candidate enters a valid OTP and clicks Next
    And Candidate Login to the dashboard
    And Candidate Views the Project Name on the Dashboard
    Then Candidate Views the interview status as Complete

  @WM-41
  Scenario: Verify that candidate is not able to proceed to the intro video with invalid OTP
    Given User created a form through form api
    And User calls the skill test questions api
    And User calls the skill test api
    And User calls the project creation api
    And User calls the api for adding a form into the project
    And User calls project activation api
    And Candidate launches the interview link
    Then Candidate should see the explainer video
    And Candidate navigates through the welcome page with valid credentials
    Then Candidate Validates the Project name on the Dashboard
    And Candidate enters an invalid OTP and clicks Next
    Then Candidate views the Invalid OTP Error message

  @WM-42
  Scenario: Intentional Failure Case - Verify that candidate is able to proceed to the intro video with OTP
    Given User created a form through form api
    And User calls the skill test questions api
    And User calls the skill test api
    And User calls the project creation api
    And User calls the api for adding a form into the project
    And User calls project activation api
    And Candidate launches the interview link
    Then Candidate should see the explainer video
    And Candidate navigates through the welcome page with valid credentials
    Then Candidate Validates the Project name on the Dashboard
    And Candidate enters an invalid OTP and clicks Next
    Then Candidate should see the Intro video and proceed