@project_creation
Feature: Test Wamly application project creation through API
  Verify that the functionalities in Wamly application is working correctly

  Background: Verify login
    Given User Login to the application with valid credentials
    Then User should be logged in successfully

  @WM-43
  Scenario: Verify that user is able to create a project through api
    Given User created a form through form api
    And User calls the skill test questions api
    And User calls the skill test api
    And User calls the project creation api
    And User calls the api for adding a form into the project
    And User calls project activation api