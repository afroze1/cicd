package com.wamly.testing.automationcore.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.utils.TestLog;

import java.util.List;

public class TestDataCollection {

    @JsonProperty("page.name")
    private String pageName;
    @JsonProperty("testdatas")
    private List<TestData> testdatas;

    public TestData getTestdata(String testdatavalue){
        if (testdatas==null)
            throw new FrameworkException("Testdata Initialization error.");
        for (TestData testdata : testdatas) {
            if (testdata.testDataName.equals(testdatavalue))
                return  testdata;
        }
        TestLog.log().error("Could not find the testdata {} in the testdata file {}",testdatavalue,pageName);
        throw new FrameworkException(String.format("Could not find the testdata %s in the testdata file %s",testdatavalue,pageName));
    }
}
