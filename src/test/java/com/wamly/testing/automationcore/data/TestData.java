package com.wamly.testing.automationcore.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestData {

    @JsonProperty("testdata.name")
    public String testDataName;
    @JsonProperty("testdata.value")
    public String testDataValue;
}
