package com.wamly.testing.automationcore.pages.ui.interviewGuides;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.awaitility.Awaitility;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import java.io.IOException;
import java.time.ZonedDateTime;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

public class interviewGuidesPage extends Page {


    int totalTimeInSec =0;

    public interviewGuidesPage(ParentDriver driver)  {
        super(driver);
        setLocatorCollection();
    }

    public void navigatetoInterviewGuides() {
        getElement("InterviewGuidesTab").click();
    }


    public String[] interviewGuidesOptions() {


        String interviewGuidesTitle = getValueFromJson("test","InterviewGuideName");
        getElement("InterviewGuideOptions",interviewGuidesTitle).click();

        String[] uiMoreOptions = new String[4];
        for(int optionPos = 1; optionPos<=4 ; optionPos++)
        {
            uiMoreOptions[optionPos-1] = getElement("MoreButtonOptions", String.valueOf((optionPos))).getText();
        }
        return uiMoreOptions;
    }


    public String getInterviewGuideTitle()
    {
        return getValueFromJson("test","InterviewGuideName");

    }

    public boolean[] interviewGuideActions(String option) throws IOException {

        String interviewGuidesTitle = getInterviewGuideTitle();
        getElement("InterviewGuideOptions",interviewGuidesTitle).click();

        boolean verifyFlags[] = new boolean[4];
        switch(option)
        {
            case "Duplicate":
                verifyFlags = duplicateOption();
                break;

            case "Edit":
                verifyFlags = editOption();
                break;

            case "Deactivate":
                verifyFlags = deactivateOption();
                break;

            case "Delete":
                verifyFlags = deleteOption();
                break;


        }
        return verifyFlags;
    }

    public boolean[] duplicateOption()
    {

        String interviewGuidesTitle = getInterviewGuideTitle();
        String duplicatedTitle = "Copy of " + interviewGuidesTitle;
        System.out.println("duplicated"+duplicatedTitle);
        getElement("SpecificMoreButton1", String.valueOf(1)).click();

        boolean[] dupVisChecks = new boolean[5];
        dupVisChecks[0] = visibilityCheck(getElement("DuplicateInterviewGuideConfirmTopic"));

        dupVisChecks[1] = visibilityCheck(getElement("DuplicateCancelButton"));
        dupVisChecks[2] = visibilityCheck(getElement("DuplicateConfirmButton"));
        getElement("DuplicateCancelButton").click();
        getElement("SpecificMoreButton1", String.valueOf(1)).click();
        getElement("DuplicateConfirmButton").click();
        getElement("InterviewGuideOptions",interviewGuidesTitle).click();
        dupVisChecks[3] = visibilityCheck(getElement("DuplicateSuccessMessage"));
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);

        await().until(() -> {
            refreshPage();
            return (getElementText("SpecificInterviewGuideFromList",duplicatedTitle).equals(duplicatedTitle));
        });
        dupVisChecks[4] = visibilityCheck(getElement("SpecificInterviewGuideFromList",duplicatedTitle));
        return dupVisChecks;
    }

    public boolean[] editOption(){
        System.out.println("Entered");
        getElement("SpecificMoreButton1", String.valueOf(2)).click();
        boolean[] editVisChecks = new boolean[8];
        editVisChecks[0] = visibilityCheck(getElement("EditInterviewGuideHeading"));
        editVisChecks[1] = visibilityCheck(getElement("InterviewGuideName"));
        editVisChecks[2] = visibilityCheck(getElement("InterviewGuideDescription"));
        editVisChecks[3] = visibilityCheck(getElement("InterviewGuideSettings"));
        editVisChecks[4] = visibilityCheck(getElement("InterviewGuideQuestions"));
        String interviewGuidesTitle = getInterviewGuideTitle();
        String appendValue = "Edited_";
        String newValue = appendValue + interviewGuidesTitle;

        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        await().until(() -> (getElement("InterviewGuideName").getAttribute("value").equals(interviewGuidesTitle)));

        getElement("InterviewGuideQuestions").click();
        getElement("InterviewGuideSettings").click();
        getElement("InterviewGuideName").clear();
        getElement("InterviewGuideName").click();
        getElement("InterviewGuideName").sendKeys(newValue);

        String editedDescriptionValue = "Edited_Description_Value";
        getElement("InterviewGuideDescription").clear();
        getElement("InterviewGuideDescription").sendKeys(editedDescriptionValue);

        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        await().until(() -> (getElement("InterviewGuideDescription").getAttribute("value").equals(editedDescriptionValue)));

        String editedQuestionValue = "Edited_Question1_Value";
        getElement("InterviewGuideQuestions").click();
        getElement("InterviewGuideQuestion", String.valueOf(1)).clear();
        getElement("InterviewGuideQuestion",String.valueOf(1)).sendKeys(editedQuestionValue);
        getElement("InterviewGuideQuestions").click();
        getElement("InterviewGuidePrepTime", String.valueOf(1)).clearTextFieldWithBackspace();
        getElement("InterviewGuidePrepTime", String.valueOf(1)).input("7");
        getElement("InterviewGuideAnswerTime", String.valueOf(1)).clearTextFieldWithBackspace();
        getElement("InterviewGuideAnswerTime", String.valueOf(1)).input("80");
        getElement("Update").click();
        updateInJson("test","InterviewGuideName", newValue);
        editVisChecks[5] = visibilityCheck(getElement("EditedSuccessfully"));
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        await().until(() -> {
            refreshPage();
            return (getElementText("SpecificInterviewGuideFromList",newValue).equals(newValue));
        });
         callTheEditOption(newValue);
        getElement("InterviewGuideQuestions").click();


        for(int i =0 ; i<=2 ;i++) {
            int questionTimeInSec = Integer.parseInt(getElement("InterviewGuidePrepTime", String.valueOf(i)).getAttribute("value"));
            int answerTimeInSec = Integer.parseInt(getElement("InterviewGuideAnswerTime", String.valueOf(i)).getAttribute("value"));
            totalTimeInSec = totalTimeInSec + questionTimeInSec + answerTimeInSec;
        }
        String[] totalTimeInfo = getTotalTime();
        String formattedTime = totalTimeInfo[0];
        String timePart = totalTimeInfo[1];
        editVisChecks[6] = (formattedTime.equals(timePart));
        getElement("Update").click();
        editVisChecks[7] = visibilityCheck(getElement("EditedSuccessfully"));

        return editVisChecks;
    }

    public void callTheEditOption(String editedValue)
    {
        getElement("InterviewGuideOptions",editedValue).click();
        getElement("SpecificMoreButton1", String.valueOf(2)).click();
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        await().until(() -> (getElement("InterviewGuideName").getAttribute("value").equals(editedValue)));

    }

    public boolean[] deactivateOption()
    {
        System.out.print("deactivate");
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        String interviewGuidesTitle = getInterviewGuideTitle();
        await().until(() -> (getElementText("SpecificInterviewGuideFromList",interviewGuidesTitle).equals(interviewGuidesTitle)));
        getElement("SpecificMoreButton1", String.valueOf(3)).click();
        boolean[] deactivateVisChecks = new boolean[10];
        deactivateVisChecks[0] = visibilityCheck(getElement("DeactivateInterviewGuideConfirmTopic"));
        deactivateVisChecks[1] = visibilityCheck(getElement("DeactivateInterviewGuideConfirmMessage"));
        deactivateVisChecks[2] = visibilityCheck(getElement("DeactivateCancelButton"));
        deactivateVisChecks[3] = visibilityCheck(getElement("DeactivateConfirmButton"));
        getElement("DeactivateCancelButton").click();
        driver.setWebDriverWaitTime(30000);
        getElement("SelectAllCheckbox").click();
        deactivateVisChecks[4] = visibilityCheck(getElement("ActivateButton"));
        deactivateVisChecks[5] = visibilityCheck(getElement("DeactivateButton"));

        getElement("DeactivateButton").click();
        deactivateVisChecks[6] = visibilityCheck(getElement("DeactivateConfirmationMessage"));
        getElement("CancelButton").click();
        deactivateVisChecks[7] = visibilityCheck(getElement("DeactivateAbortedMessage"));
        getElement("OK").click();
        getElement("DeactivateButton").click();
        getElement("OK").click();
        deactivateVisChecks[8] = visibilityCheck(getElement("SelectAllDeactivateConfirmationMessage"));
        getElement("OK").click();
        getElement("ActivateButton").click();
        deactivateVisChecks[9] = visibilityCheck(getElement("SelectAllActivateConfirmationMessage"));
        getElement("OK").click();

        return deactivateVisChecks;
    }

    public boolean[] deleteOption()
    {

        getElement("SpecificMoreButton1", String.valueOf(4)).click();
        boolean[] deleteVisChecks = new boolean[7];
        deleteVisChecks[0] = visibilityCheck(getElement("DeleteInterviewGuideConfirmTopic"));
        deleteVisChecks[1] = visibilityCheck(getElement("DeleteInterviewGuideConfirmMessage"));
        deleteVisChecks[2] = visibilityCheck(getElement("DeleteCancelButton"));
        deleteVisChecks[3] = visibilityCheck(getElement("DeleteConfirmButton"));
        getElement("DeleteCancelButton").click();
        getElement("SpecificMoreButton1", String.valueOf(4)).click();
        getElement("DeleteConfirmButton").click();
        deleteVisChecks[4] = visibilityCheck(getElement("DeleteSuccessMessage"));
        String interviewTitle = getInterviewGuideTitle();
       int count = Integer.parseInt(getElement("FullListIndex").getAttribute("aria-rowcount"));
        deleteVisChecks[5] = checkInterviewViewTitleDeletedFromList(interviewTitle,count);

        getElement("InterviewGuideTitle",interviewTitle).click();
        getElement("InterviewGuideQuestions").click();
        String Q1 = getElement("InterviewGuideQuestion","1").getText();
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        //await().until(() -> (getElement("InterviewGuideQuestion","1").getText().equals(interviewGuidesTitle)));



//        int size = getElements("InterviewGuideQuestionRowSize").size();
//        System.out.println("size"+size);
//        String questionToBeDeleted = getElement("InterviewGuideQuestions", String.valueOf(1)).getText();
//        getElement("Options").click();
//        getElement("DeleteQuestion").click();
//        getElement("Update").click();
//        deleteVisChecks[5] = visibilityCheck(getElement("DeleteSuccessMessage"));
//        deleteVisChecks[6] = checkInterviewQuestionDeletedFromList(questionToBeDeleted,size);
//        System.out.println("deleted"+ deleteVisChecks[6]);

        getElement("InterviewGuideTitle",interviewTitle).click();
        getElement("InterviewGuideQuestions").click();
        for(int i =0 ; i<=1 ;i++) {
            int questionTimeInSec = Integer.parseInt(getElement("InterviewGuidePrepTime", String.valueOf(i)).getAttribute("value"));
            int answerTimeInSec = Integer.parseInt(getElement("InterviewGuideAnswerTime", String.valueOf(i)).getAttribute("value"));
            totalTimeInSec = totalTimeInSec + questionTimeInSec + answerTimeInSec;
        }
        String[] totalTimeInfo = getTotalTime();
        String formattedTime = totalTimeInfo[0];
        String timePart = totalTimeInfo[1];
        deleteVisChecks[6] = (formattedTime.equals(timePart));




        return deleteVisChecks;
    }

    public boolean checkInterviewViewTitleDeletedFromList(String interviewTitle, int count) {

        for(int i=1; i<count; i++) {
            String listInterviewGuideName = getElement("FullList", String.valueOf(i)).getText();
            if (interviewTitle.equals(listInterviewGuideName)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkInterviewQuestionDeletedFromList(String questionToBeDeleted, int count) {

        for(int i=1;i<count;i++) {
            String listInterviewGuideName = getElement("InterviewGuideQuestions", String.valueOf(i)).getText();
            if (questionToBeDeleted.equals(listInterviewGuideName)) {
                return false;
            }
        }
            return true;
        }





    public boolean[] interviewGuideCreation(){
        navigatetoInterviewGuides();
        boolean[] createVisChecks = new boolean[18];

        createVisChecks[0] = visibilityCheck(getElement("AddNewButton"));
        getElement("AddNewButton").click();

        createVisChecks[1] = visibilityCheck(getElement("InterviewGuideName"));
        createVisChecks[2] = visibilityCheck(getElement("InterviewGuideDescription"));
        createVisChecks[3] = visibilityCheck(getElement("InterviewGuideSettings"));
        createVisChecks[4] = visibilityCheck(getElement("InterviewGuideQuestions"));
        String appendValue = String.valueOf(ZonedDateTime.now());
        String InterviewGuideName = "InterviewGuideName_Test_" + appendValue;
        updateInJson("test","InterviewGuideName", InterviewGuideName);
        String InterviewGuideDescription = "InterviewGuideDescription_Test_" + appendValue;
        String InterviewGuideQuestion = "InterviewFirstQuestion1_Test_" + appendValue;
        getElement("InterviewGuideName").sendKeys(InterviewGuideName);
        getElement("InterviewGuideDescription").sendKeys(InterviewGuideDescription);
        createVisChecks[5] = (getElement("InterviewGuideName").getAttribute("value").equals(InterviewGuideName));
        createVisChecks[6] = (getElement("InterviewGuideDescription").getAttribute("value").equals(InterviewGuideDescription));
        getElement("InterviewGuideQuestions").jsClick();
        for(int i =0 ; i<=2 ;i++) {
            getElement("clickOnAddQuestion").jsClick();
            createVisChecks[7] = visibilityCheck(getElement("InterviewGuideQuestion", String.valueOf(i)));
            createVisChecks[8] = visibilityCheck(getElement("InterviewGuidePrepTime", String.valueOf(i)));
            createVisChecks[9] = visibilityCheck(getElement("InterviewGuideAnswerTime", String.valueOf(i)));
            getElement("InterviewGuideQuestion", String.valueOf(i)).sendKeys(InterviewGuideQuestion);
            getElement("InterviewGuidePrepTime", String.valueOf(i)).clearTextFieldWithBackspace();
            getElement("InterviewGuidePrepTime", String.valueOf(i)).input("6");
            getElement("InterviewGuideAnswerTime", String.valueOf(i)).clearTextFieldWithBackspace();
            getElement("InterviewGuideAnswerTime", String.valueOf(i)).input("70");
            createVisChecks[10] = (getElement("InterviewGuideQuestion", String.valueOf(i)).getAttribute("value").equals(InterviewGuideQuestion));
            createVisChecks[11] = (getElement("InterviewGuidePrepTime", String.valueOf(i)).getAttribute("value").equals("6"));
            createVisChecks[12] = (getElement("InterviewGuideAnswerTime", String.valueOf(i)).getAttribute("value").equals("70"));
            int questionTimeInSec = Integer.parseInt(getElement("InterviewGuidePrepTime", String.valueOf(i)).getAttribute("value"));
            int answerTimeInSec = Integer.parseInt(getElement("InterviewGuideAnswerTime", String.valueOf(i)).getAttribute("value"));
            totalTimeInSec = totalTimeInSec + questionTimeInSec + answerTimeInSec;
        }

        String[] totalTimeInfo = getTotalTime();
        String formattedTime = totalTimeInfo[0];
        String timePart = totalTimeInfo[1];
        createVisChecks[13] = (formattedTime.equals(timePart));
        getElement("Create").jsClick();
        createVisChecks[14] = visibilityCheck(getElement("CreateSuccessMessage"));

        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);
        await().until(() -> {
            refreshPage();
            return (getElementText("SpecificInterviewGuideFromList",InterviewGuideName).equals(InterviewGuideName));
        });

        createVisChecks[15] = visibilityCheck(getElement("SpecificInterviewGuideFromList",InterviewGuideName));
        System.out.println(getElement("InterviewGuideQuestionNumberFromList",InterviewGuideName).getText());
        createVisChecks[16] = (getElement("InterviewGuideQuestionNumberFromList",InterviewGuideName).getText().equals("3"));
        createVisChecks[17] = (getElement("InterviewGuideStatusFromList",InterviewGuideName).getText().equals("ACTIVE"));

        return createVisChecks;
    }

    public String getElementText(String locatorName, String interviewGuideName) {
        WebElement element = getElement(locatorName, interviewGuideName);
        return element.getText();
    }

    public String[] getTotalTime(){
            int minutes = totalTimeInSec / 60;
            int seconds = totalTimeInSec % 60;
            String formattedTime = String.format("%02dm %02ds", minutes, seconds);
            String totalTime = getElement("TotalTime").getText();
            String timePart = totalTime.substring("Total time: ".length());
            return new String[]{formattedTime, timePart};

    }





}