package com.wamly.testing.automationcore.pages.api;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.pages.Base;
import com.wamly.testing.automationcore.restassured.ResponseUtils;
import com.wamly.testing.automationcore.utils.Decryption;
import com.wamly.testing.automationcore.utils.JSON;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;
import com.wamly.testing.automationcore.restassured.RestAssuredExtension;
import com.wamly.testing.automationcore.utils.FileOps;
import org.awaitility.Awaitility;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

public class API_E2E extends Base {

    public void getTokenID(String loginUrl) {
        String password = Decryption.decrypt(System.getProperty("test.password"),System.getProperty("test.securityKey"));
        String username = Decryption.decrypt(System.getProperty("test.username"),System.getProperty("test.securityKey"));
        String ClientId = Decryption.decrypt(System.getProperty("test.clientId"),System.getProperty("test.securityKey"));
        String SecretHash = Decryption.decrypt(System.getProperty("test.secretHash"),System.getProperty("test.securityKey"));
        String loginPayload = "src"+File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"apiData"+File.separator+"payload"+File.separator+"loginapi.json";
        RestAssuredExtension createUserRequest = new RestAssuredExtension(loginUrl, RestAssuredExtension.HTTPMethod.POST,"");
        String createLoginPayload = FileOps.readFileAsString(loginPayload);
        JsonNode payload = JSON.parseToJSONNode(createLoginPayload);
        JSON.updateJsonNode(payload,"ClientId",ClientId);
        JSON.updateJsonNode(payload,"AuthParameters","PASSWORD",password);
        JSON.updateJsonNode(payload,"AuthParameters","USERNAME",username);
        JSON.updateJsonNode(payload,"AuthParameters","SECRET_HASH",SecretHash);
        //Add the neccessary headers
        createUserRequest.addHeader("X-Amz-Target", "AWSCognitoIdentityProviderService.InitiateAuth");
        createUserRequest.addHeader("Content-Type", "application/x-amz-json-1.1");
        RestAssured.config = RestAssured.config()
                .encoderConfig(EncoderConfig.encoderConfig().encodeContentTypeAs("application/x-amz-json-1.1", ContentType.TEXT));
        ResponseOptions<Response> createLoginResponse = createUserRequest.executeWithBodyParams(payload);
        ResponseBody responseBody = createLoginResponse.getBody();
        int statusCode = createLoginResponse.getStatusCode();
        String tokenId = responseBody.jsonPath().getString("AuthenticationResult.IdToken");
        updateInJson("API","idToken",tokenId);
        updateInJson("API","statusToken", String.valueOf(statusCode));
    }

    public int getStatusCode(int statusCode) {
        return statusCode;
    }

    public String getDecodePwd(String strPwd) {
        byte[] decodedBytes = Base64.getDecoder().decode(strPwd);
        return new String(decodedBytes);
    }

    public void otpFetch(ZonedDateTime sentTime, String baseUrl, String EndUrl) {

        RestAssuredExtension fetchOtpRequest = new RestAssuredExtension(baseUrl+EndUrl, RestAssuredExtension.HTTPMethod.GET, "");
        String subject = awaitilityApiFetchMailSubject(fetchOtpRequest, sentTime);
        if(subject.contains("OTP"))
        {
            String otp = (subject.split("OTP just requested: ")[1]).replaceAll("[^0-9]", "");
            updateInJson("OTP","commenceOTP",otp);
        }
    }

    public String awaitilityApiFetchMailSubject(RestAssuredExtension Request, ZonedDateTime sentTime) {
        System.out.println("enetred");
        Awaitility.setDefaultTimeout(20000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1000, MILLISECONDS);
        Awaitility.setDefaultPollDelay(1000, MILLISECONDS);
        final String[] sub = new String[1];
        final ResponseBody[] responseBody = new ResponseBody[1];

        await().until(() -> {
            ResponseOptions<Response> Response = Request.execute();
            responseBody[0] = Response.getBody();
            String responseContent = responseBody[0].asString();
            System.out.println("Response Content: " + responseContent);

            ZonedDateTime receivedTime = ZonedDateTime.parse(responseBody[0].jsonPath().getString("received[0]."));
            System.out.println(receivedTime);
            System.out.println(sentTime);
            return (receivedTime.isBefore(sentTime));
        });
       sub[0] = responseBody[0].jsonPath().getString("subject");
        return sub[0];
    }

    public String candidateEmailStatusFetch(String conditionValue, String timeZoneString, String baseUrl, String EndUrl) {
        ZonedDateTime completeMailSentTime = ZonedDateTime.parse(timeZoneString);
        RestAssuredExtension fetchOtpRequest = new RestAssuredExtension(baseUrl+EndUrl, RestAssuredExtension.HTTPMethod.GET, "");
        String subject = awaitilityApiFetchMailSubject(fetchOtpRequest, completeMailSentTime);
        String returnValue = null;
        if(subject.contains(conditionValue))
        {
            returnValue = (subject.split(",")[0]).replace("[", "");
        }
        return returnValue;
    }

    public void deleteAllEmail(String baseUrl, String EndUrl) {
        RestAssuredExtension deleteEmailRequest = new RestAssuredExtension(baseUrl + EndUrl, RestAssuredExtension.HTTPMethod.DELETE, "");
        ResponseOptions<Response> fetchOtpResponse = deleteEmailRequest.execute();
    }

    public void createForm(String loginToken, String baseUrl, String endpointUrl) {
        String formPayload = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "apiData" + File.separator + "payload" + File.separator + "formapi.json";
        RestAssuredExtension createUserRequest = new RestAssuredExtension(baseUrl + endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        updatePayload("name", "", "formapi");
        String createFormPayload = FileOps.readFileAsString(formPayload);
        ResponseOptions<Response> createFormResponse = createUserRequest.executeWithBodyParams(createFormPayload);
        ResponseBody responseBody = createFormResponse.getBody();
        String formId = responseBody.jsonPath().getString("response.id");
        updateInJson("API", "formid", formId);
    }
    public JsonNode getForm(String loginToken, String baseUrl, String endpointUrl) {
        RestAssuredExtension formGetRequest = new RestAssuredExtension(baseUrl + endpointUrl, RestAssuredExtension.HTTPMethod.GET, loginToken);
        ResponseOptions<Response> formResponse = formGetRequest.execute();
        ResponseBody responseBody = formResponse.getBody();
        return ResponseUtils.readValueAsJsonNodeFromJsonString(responseBody.asString(),"response");
    }

    public void createSkillTestQuestions(String loginToken,String baseUrl,String endpointUrl) {
        String commonPath = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "apiData" + File.separator + "payload" + File.separator;
        String skillTestQ1Payload = commonPath + "skillTestQuestion1.json";
        String skillTestQ2Payload = commonPath + "skillTestQuestion2.json";
        String skillTestQ3Payload = commonPath + "skillTestQuestion3.json";

        RestAssuredExtension createSkillTestQ1Request = new RestAssuredExtension(baseUrl + endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        updatePayload("title", "", "skillTestQuestion1");
        updatePayload("value", "html", "skillTestQuestion1");
        String createSkillTestQ1Payload = FileOps.readFileAsString(skillTestQ1Payload);
        ResponseOptions<Response> skillTestQ1Response = createSkillTestQ1Request.executeWithBodyParams(createSkillTestQ1Payload);
        ResponseBody responseBodyQ1 = skillTestQ1Response.getBody();
        String skillTestQ1Id = responseBodyQ1.jsonPath().getString("response.id");
        updateInJson("API", "skillTestQ1Id", skillTestQ1Id);

        RestAssuredExtension createSkillTestQ2Request = new RestAssuredExtension(baseUrl + endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        updatePayload("title", "", "skillTestQuestion2");
        updatePayload("value", "html", "skillTestQuestion2");
        String createSkillTestQ2Payload = FileOps.readFileAsString(skillTestQ2Payload);
        ResponseOptions<Response> skillTestQ2Response = createSkillTestQ2Request.executeWithBodyParams(createSkillTestQ2Payload);
        ResponseBody responseBodyQ2 = skillTestQ2Response.getBody();
        String skillTestQ2Id = responseBodyQ2.jsonPath().getString("response.id");
        updateInJson("API", "skillTestQ2Id", skillTestQ2Id);

        RestAssuredExtension createSkillTestQ3Request = new RestAssuredExtension(baseUrl + endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        updatePayload("title", "", "skillTestQuestion3");
        updatePayload("value", "html", "skillTestQuestion3");
        String createSkillTestQ3Payload = FileOps.readFileAsString(skillTestQ3Payload);
        ResponseOptions<Response> skillTestQ3Response = createSkillTestQ3Request.executeWithBodyParams(createSkillTestQ3Payload);
        ResponseBody responseBodyQ3 = skillTestQ3Response.getBody();
        String skillTestQ3Id = responseBodyQ3.jsonPath().getString("response.id");
        updateInJson("API", "skillTestQ3Id", skillTestQ3Id);
    }

    public JsonNode createSkillTest(String loginToken, String baseUrl, String endpointUrl, String skillTestQ1Id, String skillTestQ2Id, String skillTestQ3Id) {
        String skillTestPayload = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "apiData" + File.separator + "payload" + File.separator + "skillTest.json";

        String[] newIds = {skillTestQ1Id,skillTestQ2Id,skillTestQ3Id};
        UpdateJsonIds("questions","id",newIds,"skillTest");
        updatePayload("name", "", "skillTest");

        RestAssuredExtension createSkillTestRequest = new RestAssuredExtension(baseUrl+endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        String createSkillTestPayload = FileOps.readFileAsString(skillTestPayload);
        ResponseOptions<Response> skillTestResponse = createSkillTestRequest.executeWithBodyParams(createSkillTestPayload);
        if(skillTestResponse.getStatusCode() != 200){
            throw new FrameworkException("Skill test creation api getting status code as: "+skillTestResponse.getStatusCode()+" and its response body is "+skillTestResponse.getBody().asString());
        }

        ResponseBody responseBody = skillTestResponse.getBody();
        String skillTestId = responseBody.jsonPath().getString("response.id");
        updateInJson("API","skillTestId",skillTestId);
        return ResponseUtils.readValueAsJsonNodeFromJsonString(responseBody.asString(),"response");
    }
    public void createProject(String loginToken,String baseUrl,String endpointUrl,JsonNode skillTestResponse) {
        updatePayload("projectName", "", "createProject");
        updatePayload("description", "", "createProject");

        String fileContent = getPayloadContent("createProject");
        JsonNode payload = JSON.parseToJSONNode(fileContent);
        JSON.updateJsonNode(payload,"assessmentDTO",skillTestResponse);
        JSON.updateJsonNode(payload,"startDate",today());

        RestAssuredExtension createProjectRequest = new RestAssuredExtension(baseUrl+endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        ResponseOptions<Response> projectCreationResponse = createProjectRequest.executeWithBodyParams(payload);
        if(projectCreationResponse.getStatusCode() != 201){
            throw new FrameworkException("Project creation api getting status code as: "+projectCreationResponse.getStatusCode()+" and its response body is "+projectCreationResponse.getBody().asString());
        }

        ResponseBody responseBody = projectCreationResponse.getBody();
        String ProjectCreationId = responseBody.jsonPath().getString("response.projectId");
        String ProjectName = responseBody.jsonPath().getString("response.projectName");
        updateInJson("API","ProjectCreationId",ProjectCreationId);
        updateInJson("API","ProjectName",ProjectName);
    }

    public void updateProject(String loginToken,String baseUrl,String endpointUrl,JsonNode formResponse,JsonNode skillTestResponse,String projectName,String projectId) throws IOException {
        String fileContent = getPayloadContent("updateProject");
        JsonNode payload = JSON.parseToJSONNode(fileContent);
        JSON.updateJsonNode(payload,"projectName",projectName);
        JSON.updateJsonNode(payload,"projectId",projectId);
        JSON.updateJsonNode(payload,"formDTO",formResponse);
        JSON.updateJsonNode(payload,"assessmentDTO",skillTestResponse);
        JSON.updateJsonNode(payload,"startDate",today());

        RestAssuredExtension createProjectRequest = new RestAssuredExtension(baseUrl+endpointUrl, RestAssuredExtension.HTTPMethod.PUT, loginToken);
        ResponseOptions<Response> projectCreationResponse = createProjectRequest.executeWithBodyParams(payload);
        ResponseBody responseBody = projectCreationResponse.getBody();
        String updateProjectId = responseBody.jsonPath().getString("response.projectId");
        updateInJson("API","updateProjectId",updateProjectId);
    }

    public void activateProject(String loginToken,String baseUrl,String endpointUrl){
        RestAssuredExtension activateProjectRequest = new RestAssuredExtension(baseUrl+endpointUrl, RestAssuredExtension.HTTPMethod.POST, loginToken);
        ResponseOptions<Response> projectActivationResponse = activateProjectRequest.execute();
        ResponseBody responseBody = projectActivationResponse.getBody();
        String ProjectLink = responseBody.jsonPath().getString("response.genericLink");
        updateInJson("API","ProjectGenericLink",ProjectLink);
        System.out.println("ProjectLink:"+ProjectLink);
    }

    protected static String randomNum(){
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return now.format(formatter);
    }

    public String today(){
        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return today.format(formatter);
    }

    public String getPayloadContent(String file){
        String fileName = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "apiData" + File.separator + "payload" + File.separator + file + ".json";
        String jsonContent = null;
        try {
            jsonContent = readJsonFile(fileName);
        } catch (Exception e) {
            System.out.println(e);
        }
        return jsonContent;
    }

    public void updatePayload(String key,String value,String file) {
        String fileName = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"apiData"+File.separator+"payload"+File.separator+file+".json";
        String updatedValue;
        try {
            String jsonContent = readJsonFile(fileName);
            JSONObject jsonObject = new JSONObject(jsonContent);
            String existingValue = jsonObject.getString(key);
            if(value.equals("html")){
                existingValue = existingValue.replaceAll("<[^>]*>", "");
            }
            updatedValue = existingValue.substring(0, existingValue.lastIndexOf("_") + 1) + randomNum();
            if(value.equals("html")){
                updatedValue = "<p>"+updatedValue+"</p>";
            }
            jsonObject.put(key, updatedValue);
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(jsonObject.toString());
            fileWriter.close();
        }catch (IOException e) {
            System.out.println("An error occurred while reading/writing the file: " + e.getMessage());
        } catch (JSONException e) {
            System.out.println("Error while parsing JSON: " + e.getMessage());
        }
    }

    public void UpdateJsonIds(String node,String key,String[] values,String file) {
        String fileName = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"apiData"+File.separator+"payload"+File.separator+file+".json";

        ObjectMapper objectMapper = new ObjectMapper();
        File jsonFile = new File(fileName);
        int valuesIndex = 0;

        try {
            // Read the JSON file into a JsonNode
            JsonNode jsonNode = objectMapper.readTree(jsonFile);

            // Iterate through the 'questions' array and update id values
            if (jsonNode.has(node) && jsonNode.get(node).isArray()) {
                for (JsonNode nodeKey : jsonNode.get(node)) {
                    if (nodeKey.has(key)) {
                        // Generate a new ID (You can implement your logic to generate new IDs here)
                        String updateValue = values[valuesIndex];

                        // Update the 'id' field with the new value
                        ((ObjectNode) nodeKey).put(key, updateValue);
                        valuesIndex = (valuesIndex + 1) % values.length;
                    }
                }
            }
            // Write the updated JSON back to the file
            objectMapper.writeValue(jsonFile, jsonNode);
            System.out.println("IDs updated successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getEmail(ZonedDateTime sentTime, String baseUrl, String EndUrl) {
        boolean[] VisEmailCheck = new boolean[5];
        RestAssuredExtension fetchOtpRequest = new RestAssuredExtension(baseUrl + EndUrl, RestAssuredExtension.HTTPMethod.GET, "");
        ResponseOptions<Response> formResponse = fetchOtpRequest.execute();
        ResponseBody responseBody = formResponse.getBody();
        String actualText = awaitilityApiFetchMailSubject(fetchOtpRequest, sentTime);
        String messageId = responseBody.jsonPath().getString("_id");
        String extractedMessageId = messageId.replaceAll("\\[|\\]", "");
        System.out.println(extractedMessageId);
        RestAssuredExtension htmlcontent = new RestAssuredExtension("https://mailsac.com/api/text/ciya@mailsac.com/hvglDB4LcPQ08NLEVfMi4tJS19SoE-0?_mailsacKey=k_z3Uy5fRoiWcTgDC4DZWsGdPXGBQlStlaqw5f51", RestAssuredExtension.HTTPMethod.GET, "");
        ResponseOptions<Response> formResponse1 = htmlcontent.execute();
        ResponseBody responseBody1 = formResponse1.getBody();
        System.out.println("Response Body: " + responseBody1.asString());
        String fullText = responseBody1.asString();
        String organization = extractValue(fullText, "Organisation: (.*?)\\n");
        String link = extractValue(fullText, "please click on the following unique link:\\s*([^\\s]+)\\s*\\[");
        String Role = extractValue(fullText, "Role: (.*?)\\n");
        String Name = extractValue(fullText, "Hello (.*?) You");


        System.out.println(organization);
        System.out.println(Name);
        System.out.println(Role);
        System.out.println(link);
        String[] values = {Name,Role,link};
        return values;


    }

    private static String extractValue(String content, String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(content);

        if (matcher.find()) {
            return matcher.group(1).trim();
        }

        return null;
    }

    private static boolean visibilityOfTextMatches(String actualText, String targetText) {

        return actualText != null && targetText != null && actualText.equals(targetText);
    }
}
