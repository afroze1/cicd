package com.wamly.testing.automationcore.pages;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.json.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * All Global Objects and Actions in the Web Application is maintained here.
 */
public class Base  {

    public String fileName = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"testdata"+File.separator+"runTimeData"+File.separator+"testdata.json";



    public String readJsonFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }

    public void updateInJson(String module,String key, String updatedValue) {
        try {
            // Read the existing JSON content
            String jsonContent = readJsonFile(fileName);

            // Parse the JSON content into a JSONObject
            JSONObject jsonObject = new JSONObject(jsonContent);

            // Update the value
            jsonObject.getJSONObject(module).put(key, updatedValue);

            // Write the updated JSON back to the file
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(jsonObject.toString());
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new FrameworkException(module+" - "+key +" is not updated in the json file");
        }
    }

}
