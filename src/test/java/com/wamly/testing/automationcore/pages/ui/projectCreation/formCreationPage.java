package com.wamly.testing.automationcore.pages.ui.projectCreation;


import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.apache.commons.lang.RandomStringUtils;
import org.awaitility.Awaitility;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

public class formCreationPage extends Page {

    public formCreationPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void formsPageOpen() {
        getElement("Forms").click();
    }

    public boolean formsTrainingVideo() {
        getElement("TrainingVideoLink").click();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

        //Switch newly open Tab
        driver.getDriver().switchTo().window(tabs.get(1));

        //Perform some operation on Newly open tab
//        driver.getDriver().switchTo().frame(getElement("VideoIframe"));
        switchFrameToIndex(0);
        getElement("PlayButton").click();
        getElement("VideoContainer").click();
        boolean videoVisibility = getElement("VideoContainer").isEnabled();
        //Close newly open tab after performing some operations.
        driver.getDriver().switchTo().window(tabs.get(1)).close();

        //Switch to old tab.
        driver.getDriver().switchTo().window(tabs.get(0));

        return videoVisibility;
    }

    public String[] formsOptions()
    {
        driver.setWebDriverWaitTime(30000);
        String formTitle = getFormTitle("1");
        int rowIndexValue = moreButtonClick(formTitle);
        String[] uiMoreOptions = new String[5];
        for(int optionPos = 1; optionPos<=5 ; optionPos++)
        {
            uiMoreOptions[optionPos-1] = getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+optionPos))).getText();
        }
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        getElement("SpecificMoreButton",formTitle).click();

        Actions action = new Actions(driver.getDriver());
        action.doubleClick(getElement("SpecificMoreButton",formTitle)).perform();

        return uiMoreOptions;
    }

    public int moreButtonClick(String formTitle)
    {
        int rowIndex = Integer.parseInt(getElement("FormRow",formTitle).getAttribute("row-index"));
        getElement("SpecificMoreButton",formTitle).jsClick();
        return rowIndex;
    }

    public String getFormTitle(String titleIndex)
    {
        return getElement("FormTitle",titleIndex).getText();
    }

    public boolean[] formsActions(String option) {
        driver.setWebDriverWaitTime(30000);
        String formTitle = getFormTitle("1");
        int rowIndexValue = moreButtonClick(formTitle);
        boolean verifyFlags[] = new boolean[5];
        switch(option)
        {
            case "Set As Default":
                verifyFlags[0] = setAsDefaultOption(rowIndexValue, formTitle);
                break;
            case "Duplicate":
                verifyFlags = duplicateOption(rowIndexValue);
                break;
            case "Edit":
                verifyFlags = editOption(rowIndexValue);
                break;
            case "Deactivate":
                verifyFlags = deactivateOption(rowIndexValue);
                break;
            case "Delete":
                verifyFlags = deleteOption(rowIndexValue);
                break;
        }
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return verifyFlags;
    }

    public boolean setAsDefaultOption(int rowIndexValue, String formTitle)
    {
        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+1))).click();
        return visibilityCheck(getElement("SetAsDefaultSuccessMessage",formTitle));
    }

    public boolean[] duplicateOption(int rowIndexValue)
    {
        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+2))).click();
        boolean[] dupVisChecks = new boolean[5];
        dupVisChecks[0] = visibilityCheck(getElement("DuplicateFormConfirmTopic"));
        dupVisChecks[1] = visibilityCheck(getElement("DuplicateFormConfirmMessage"));
        dupVisChecks[2] = visibilityCheck(getElement("DuplicateCancelButton"));
        dupVisChecks[3] = visibilityCheck(getElement("DuplicateConfirmButton"));
        getElement("DuplicateCancelButton").click();

        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+2))).click();
        getElement("DuplicateConfirmButton").click();
        dupVisChecks[4] = visibilityCheck(getElement("DuplicateSuccessMessage"));

        return dupVisChecks;
    }

    public void gotoFormBuilder()
    {
        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(1000, MILLISECONDS);
        getElement("FormBuilder").click();

        await().until(() -> {
            getElement("FormSettings").click();
            getElement("FormBuilder").click();
            return getElement("SortableItem").isDisplayed();
        });
    }

    public boolean[] editOption(int rowIndexValue){

        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+3))).click();
        boolean[] editVisChecks = new boolean[6];
        editVisChecks[0] = visibilityCheck(getElement("FormName"));
        editVisChecks[1] = visibilityCheck(getElement("FormDescription"));
        editVisChecks[2] = visibilityCheck(getElement("FormSettings"));
        editVisChecks[3] = visibilityCheck(getElement("FormBuilder"));

        String appendValue = "_Edit_"+String.valueOf(ZonedDateTime.now());
        getElement("FormName").input(appendValue);
        getElement("FormSettings").click();
        getElement("FormDescription").input(appendValue);
        getElement("FormSettings").click();
        editVisChecks[4] = (getElement("FormName").getAttribute("value").contains(appendValue));
        editVisChecks[5] = (getElement("FormDescription").getAttribute("value").contains(appendValue));

        getElement("CancelButton").click();
        return editVisChecks;
    }

    public boolean[] deactivateOption(int rowIndexValue)
    {
        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+4))).click();
        boolean[] deactivateVisChecks = new boolean[4];
        deactivateVisChecks[0] = visibilityCheck(getElement("DeactivateFormConfirmTopic"));
        deactivateVisChecks[1] = visibilityCheck(getElement("DeactivateFormConfirmMessage"));
        deactivateVisChecks[2] = visibilityCheck(getElement("DeactivateCancelButton"));
        deactivateVisChecks[3] = visibilityCheck(getElement("DeactivateConfirmButton"));
        getElement("DeactivateCancelButton").click();

//        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+4))).click();
//        getElement("DeactivateConfirmButton").click();
//        deactivateVisChecks[4] = visibilityCheck(getElement("DeactivateSuccessMessage"));

        return deactivateVisChecks;
    }

    public boolean[] deleteOption(int rowIndexValue)
    {
        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+5))).click();
        boolean[] deleteVisChecks = new boolean[4];
        deleteVisChecks[0] = visibilityCheck(getElement("DeleteFormConfirmTopic"));
        deleteVisChecks[1] = visibilityCheck(getElement("DeleteFormConfirmMessage"));
        deleteVisChecks[2] = visibilityCheck(getElement("DeleteCancelButton"));
        deleteVisChecks[3] = visibilityCheck(getElement("DeleteConfirmButton"));
        getElement("DeleteCancelButton").click();

//        getElement("MoreButtonOptions", String.valueOf(((rowIndexValue*5)+5))).click();
//        getElement("DeleteConfirmButton").click();
//        deleteVisChecks[4] = visibilityCheck(getElement("DeleteSuccessMessage"));

        return deleteVisChecks;
    }

    public boolean[] formCreation(){

        boolean[] createVisChecks = new boolean[7];

        createVisChecks[0] = visibilityCheck(getElement("AddNewButton"));
        getElement("AddNewButton").click();

        createVisChecks[1] = visibilityCheck(getElement("FormName"));
        createVisChecks[2] = visibilityCheck(getElement("FormDescription"));
        createVisChecks[3] = visibilityCheck(getElement("FormSettings"));
        createVisChecks[4] = visibilityCheck(getElement("FormBuilder"));

        String appendValue = String.valueOf(ZonedDateTime.now());
        String formName = "FormName_Test_" + appendValue;
        String formDescription = "FormDescription_Test_" + appendValue;
        getElement("FormName").sendKeys(formName);
        getElement("FormDescription").sendKeys(formDescription);
        createVisChecks[5] = (getElement("FormName").getAttribute("value").equals(formName));
        createVisChecks[6] = (getElement("FormDescription").getAttribute("value").equals(formDescription));
        return createVisChecks;
    }

    public boolean[] formLabelsToolsCheck() throws IOException {
        gotoFormBuilder();
        boolean[] createLabelsCheck = new boolean[19];

        String[] reqCandidateDetails = getFormsJsonValue("RequiredCandidateDetailsLabel").split(",");
        int pos = 0;
        for (String detail : reqCandidateDetails) {
            createLabelsCheck[pos] = visibilityCheck(getElement("SpecificRequiredCandidateLabel", detail));
            pos++;//4
        }

        String[] toolBoxCandidateDetails = getFormsJsonValue("ToolBoxOptions").split(",");
        for (String canLabel : toolBoxCandidateDetails) {
            createLabelsCheck[pos] = visibilityCheck(getElement("CandidateToolBoxModifiable", canLabel));
            pos++;//19
        }
        return createLabelsCheck;
    }

    public void addAllFields() throws IOException {

        String[] toolBoxCandidateDetails = getFormsJsonValue("ToolBoxOptions").split(",");
        for (String canLabel : toolBoxCandidateDetails) {
            getElement("CandidateToolBoxModifiable", canLabel).click();
        }
        String[] editableLabel = getFormsJsonValue("EditableLabels").split(",");
        for (String editLabel : editableLabel) {
            getElement("ModifiableEditButton",editLabel).click();
            getElement("DisplayLabelEdit").jsClick();
            getElement("DisplayLabelEdit").sendKeys(RandomStringUtils.random(5, true, true)+"_");

            if(editLabel.equals("Dropdown")||editLabel.equals("Check Box/Multi-select"))
            {
                List<Element> dropdownOptions = getElements("DropdownOptionsInput");
                for(Element option: dropdownOptions)
                {
                    option.sendKeys("_"+RandomStringUtils.random(5, true, true));
                }
            }
            getElement("RequiredCheckBox").click();
            getElement("RequiredCheckBox").click();
            getElement("ModifiableCloseButton",editLabel).click();
        }
    }

    public boolean createForm(){
        getElement("UpdateButton").jsClick();
        return visibilityCheck(getElement("UpdateSuccessMessage"));
    }
}
