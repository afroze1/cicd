package com.wamly.testing.automationcore.pages.ui.user;

import com.wamly.testing.automationcore.pages.Base;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;

import java.io.IOException;

public class addUserPage extends Page {

    public addUserPage(ParentDriver driver){
        super(driver);
        setLocatorCollection();

    }

    public boolean[] createANewUser() throws IOException {
        boolean verifyFlags[] = new boolean[2];

        getElement("UsersTab").click();
        getElement("AddUser").click();
        getElement("FirstName").input("AutomationUser");
        getElement("LastName").input("Test");
        getElement("Email").input("automationuser@mailsac.com");
        getElement("Phone").input("+27123456789");
        getElement("Role").click();
        String roleName = getElement("RoleName").getAttribute("value");

        //verifyFlags[0] = visibilityCheck(getElement("AddUser"));
        //verifyFlags[1] = visibilityCheck(getElement("SuccessMessage"));
      String firstName =  getElement("FirstName").getAttribute("value");

        Base json = new Base();
        json.updateInJson("addUser","FirstName",firstName);
        json.updateInJson("addUser","roleName",roleName);
        return verifyFlags;

    }




}
