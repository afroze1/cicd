package com.wamly.testing.automationcore.pages.ui.projectCreation;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.pages.Base;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.awaitility.Awaitility;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

public class skillsTestCreationPage extends Page {

    public skillsTestCreationPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void skillsTestPageOpen() {
        getElement("SkillsTests").click();
    }

    public boolean skillsTestTrainingVideo() {
        getElement("TrainingVideoLink").click();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

        //Switch newly open Tab
        driver.getDriver().switchTo().window(tabs.get(1));

        //Perform some operation on Newly open tab
//        driver.getDriver().switchTo().frame(getElement("VideoIframe"));
        switchFrameToIndex(0);
        getElement("PlayButton").click();
        getElement("VideoContainer").click();
        boolean videoVisibility = getElement("VideoContainer").isEnabled();
        //Close newly open tab after performing some operations.
        driver.getDriver().switchTo().window(tabs.get(1)).close();

        //Switch to old tab.
        driver.getDriver().switchTo().window(tabs.get(0));

        return videoVisibility;
    }

    public boolean skillsTestIsInitialPage() {
        return (getElements("AddNewButton").size() > 1);
    }

    public boolean[] skillsTestInitialPageCheck() {
        boolean[] skillsTestInitChecks = new boolean[3];
        skillsTestInitChecks[0] = visibilityCheck(getElement("CreateFirstQnMessage"));
        skillsTestInitChecks[1] = visibilityCheck(getElement("CreateFirstQnMsgDescription"));
        skillsTestInitChecks[2] = visibilityCheck(getElement("AddNewButtonInit"));
        return skillsTestInitChecks;
    }

    public boolean[] skillsTestPageCheck() {

        driver.setWebDriverWaitTime(30000);
        boolean[] skillsTestVisChecks = new boolean[6];

        skillsTestVisChecks[0] = visibilityCheck(getElement("SkillsTestPageGeneral", "QUESTION"));
        skillsTestVisChecks[1] = visibilityCheck(getElement("SkillsTestPageGeneral", "NO. OF ANSWERS"));
        skillsTestVisChecks[2] = visibilityCheck(getElement("SkillsTestPageGeneral", "TIME TO ANSWER"));
        skillsTestVisChecks[3] = visibilityCheck(getElement("SkillsTestPageGeneral", "NO. OF TEMPLATES LINKED"));
        skillsTestVisChecks[4] = visibilityCheck(getElement("SkillsTestPageGeneral", "Add New"));
        skillsTestVisChecks[5] = visibilityCheck(getElement("SearchQuestionsTab"));

        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return skillsTestVisChecks;
    }

    public boolean[] createSkillsQuestionPageCheck() {

        getElement("AddNewButton").click();
        boolean[] skillsQnVisChecks = new boolean[9];
        driver.setWebDriverWaitTime(30000);
        skillsQnVisChecks[0] = visibilityCheck(getElement("SkillsQnPageTopic"));
        skillsQnVisChecks[1] = visibilityCheck(getElement("SkillsQnPageQuestionButton"));
        skillsQnVisChecks[2] = visibilityCheck(getElement("SkillsQnPageAnswersButton"));
        skillsQnVisChecks[3] = visibilityCheck(getElement("SkillsQnPageGeneral", "Cancel"));
        skillsQnVisChecks[4] = visibilityCheck(getElement("PreviewSkillQnButton"));
        skillsQnVisChecks[5] = visibilityCheck(getElement("CreateSkillQnButton"));
        skillsQnVisChecks[6] = visibilityCheck(getElement("SkillQnTitle"));
        skillsQnVisChecks[7] = visibilityCheck(getElement("SkillQnAnswerTimeMin"));
        skillsQnVisChecks[8] = visibilityCheck(getElement("SkillQnContentFrame"));

        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return skillsQnVisChecks;
    }

    public boolean[] createSkillsQuestionsCheck(String imageUrl) {

        boolean[] skillsQnVisChecks = new boolean[21];
        String appendValue = "Test_" + ZonedDateTime.now();
        String qnTitle = "Title_" + appendValue;
        String qnContent = "Question_" + appendValue;

        System.arraycopy(skillQuestionSectionCheck(qnTitle, appendValue, qnContent, imageUrl), 0, skillsQnVisChecks, 0, 2);

        int noOfAnswers = generateRandomInteger(2, 7);
        System.arraycopy(skillQnAnswerSectionCheck(noOfAnswers), 0, skillsQnVisChecks, 2, 8);

        List<Element> addImageButtons = getElements("AddImageButton");

        String ansContentText = null;
        for (int ansFields = 1; ansFields <= noOfAnswers; ansFields++) {
            switchFrameToIndex(ansFields - 1);
            ansContentText = "Answer_" + ansFields + "_" + appendValue;
            String qnDataID = getElement("SkillAnsContentIdentifier").getAttribute("data-id");
            getElement("SkillQnAnsContentNoText").sendKeys(ansContentText);

            skillsQnVisChecks[10] = getElement("SkillAnsContent", qnDataID).getText().contains(ansContentText);
            driver.getDriver().switchTo().defaultContent();
            //paste image
            addImageButtons.get(ansFields - 1).jsClick();
            getElement("AddImageUrl").input(imageUrl);
            getElement("ConfirmSaveButton").jsClick();
        }

        getElement("SkillsQnPageQuestionButton").click();
        getElement("SkillsQnPageAnswersButton").click();

        skillsQnVisChecks[11] = visibilityCheck(getElement("PreviewSkillQnButton"));
        getElement("PreviewSkillQnButton").jsClick();

        System.arraycopy(skillQnPreviewSectionCheck(noOfAnswers, qnContent, ansContentText), 0, skillsQnVisChecks, 12, 8);

        getElement("CreateSkillQnButton").jsClick();
        skillsQnVisChecks[20] = visibilityCheck(getElement("divElement", qnTitle));

        Base json = new Base();
        json.updateInJson("SkillQuestion", "qnTitle", qnTitle);
        json.updateInJson("SkillQuestion", "appendValue", appendValue);
        json.updateInJson("SkillQuestion", "noOfAnswers", String.valueOf(noOfAnswers));
        return skillsQnVisChecks;
    }

    public boolean[] skillQuestionSectionCheck(String qnTitle, String appendValue, String qnContent, String imageUrl) {

        Base json = new Base();
        boolean[] skillsQnSection = new boolean[2];
        getElement("SkillQnTitle").input(qnTitle);

        getElement("SkillQnAnswerTimeMin").clear();
        String min = String.valueOf(generateRandomInteger(1, 10));
        getElement("SkillQnAnswerTimeMin").input(min);
        json.updateInJson("SkillQuestion", "answerTimeMin", min);

        getElement("SkillQnAnswerTimeSec").clear();
        String sec = String.valueOf(generateRandomInteger(1, 59));
        getElement("SkillQnAnswerTimeSec").input(sec);
        json.updateInJson("SkillQuestion", "answerTimeSec", sec);

        skillsQnSection[0] = (getElement("SkillQnTitle").getAttribute("value").contains(appendValue));

        switchFrameToIndex(0);
        getElement("SkillQnAnsContent").sendKeys(qnContent);
        skillsQnSection[1] = (getElement("SkillQnAnsContentText").getText().contains(appendValue));

        driver.getDriver().switchTo().defaultContent();

        //Paste image
        getElement("AddImageButton").jsClick();
        getElement("AddImageUrl").input(imageUrl);
        getElement("ConfirmSaveButton").jsClick();

        return skillsQnSection;
    }

    public boolean[] skillQnAnswerSectionCheck(int noOfAnswers) {
        boolean[] skillsQnAnsSection = new boolean[8];
        skillsQnAnsSection[0] = visibilityCheck(getElement("SkillsQnPageAnswersButton"));
        getElement("SkillsQnPageAnswersButton").click();
        skillsQnAnsSection[1] = (getElement("SkillQnNoOfAnswers").getText().contains("2"));
        skillsQnAnsSection[2] = (getElement("SkillQnCorrectAnswerOption").getText().contains("1"));
        skillsQnAnsSection[3] = visibilityCheck(getElement("SkillQnAnswerContent", "1"));
        skillsQnAnsSection[4] = visibilityCheck(getElement("SkillQnAnswerContent", "2"));

        System.arraycopy(skillQnAnswerSelectionCheck(noOfAnswers), 0, skillsQnAnsSection, 5, 3);

        return skillsQnAnsSection;
    }

    public boolean[] skillQnAnswerSelectionCheck(int noOfAnswers)
    {
        boolean[] skillsQnAnsSelection = new boolean[3];
        getElement("SkillQnNoOfAnswers").click();
        List<Element> dropdownOptions = getElements("AllNoOfAnswersOptions");
        for (int dropPos = 0; dropPos < 7; dropPos++) {
            skillsQnAnsSelection[0] = (dropdownOptions.get(dropPos).getText().equals(String.valueOf(dropPos + 2)));
        }
        getElement("DropDownOptions", String.valueOf(noOfAnswers)).click();

        for (int ansFields = 1; ansFields <= noOfAnswers; ansFields++) {
            skillsQnAnsSelection[1] = visibilityCheck(getElement("SkillQnAnswerContent", String.valueOf(ansFields)));
        }

        getElement("SkillQnCorrectAnswerOption").click();
        int noOfCorrectAnswers = generateRandomInteger(1, noOfAnswers);
        for (int dropPos = 1; dropPos <= noOfAnswers; dropPos++) {
            skillsQnAnsSelection[2] = visibilityCheck(getElement("AllCorrectAnswerOption", String.valueOf(dropPos)));
        }
        getElement("DropDownAnswerOptions", String.valueOf(noOfCorrectAnswers-1)).click();

        return skillsQnAnsSelection;

    }

    public boolean[] skillQnPreviewSectionCheck(int noOfAnswers, String qnContent, String ansContentText) {
        boolean[] skillsQnPreviewSection = new boolean[8];

        skillsQnPreviewSection[0] = visibilityCheck(getElement("divElement", "Desktop"));
        getElement("divElement", "Desktop").click();

        skillsQnPreviewSection[1] = getElement("SkillQuestionDesktopMobilePreview").getText().contains(qnContent);
        for (int ansFields = 1; ansFields <= noOfAnswers; ansFields++) {
            skillsQnPreviewSection[2] = getElement("SkillQnAnswersDesktopMobilePreview", String.valueOf(ansFields)).getText().contains(ansContentText);
        }

        getElement("divElement", "Mobile").jsClick();
        skillsQnPreviewSection[3] = visibilityCheck(getElement("divElement", "Mobile"));

        skillsQnPreviewSection[4] = visibilityCheck(getElement("SkillTestTimer"));

        skillsQnPreviewSection[5] = getElement("SkillQuestionDesktopMobilePreview").getText().contains(qnContent);

        for (int ansFields = 1; ansFields <= noOfAnswers; ansFields++) {
            skillsQnPreviewSection[6] = getElement("SkillQnAnswersDesktopMobilePreview", String.valueOf(ansFields)).getText().contains(ansContentText);
        }

//        skillsQnVisChecks[] = visibilityCheck(getElement("SaveAndContinueButton"));
//        getElement("SaveAndContinueButton").click();

        skillsQnPreviewSection[7] = visibilityCheck(getElement("ExitPreviewButton"));
        getElement("ExitPreviewButton").click();

        return skillsQnPreviewSection;
    }

    public boolean[] skillsTestPageSearchCheck(String qnTitle, String noOfAnswers, String min, String sec) {
        boolean[] skillsTestSearchCheck = new boolean[5];
        getElement("SearchQuestionsTab").input(qnTitle);

        skillsTestSearchCheck[0] = visibilityCheck(getElement("divElement", qnTitle));
        skillsTestSearchCheck[1] = (getElement("NoOfAnswersSearch", qnTitle).getText()).equals(noOfAnswers);
        String time = getElement("TimeSearch", qnTitle).getText();
        skillsTestSearchCheck[2] = String.valueOf(Integer.parseInt(time.split("min ")[0])).equals(min);
        skillsTestSearchCheck[3] = String.valueOf(Integer.parseInt(time.split("min ")[1].replace("s", ""))).equals(sec);
        skillsTestSearchCheck[4] = visibilityCheck(getElement("LinkedTemplateSearch", qnTitle));

        return skillsTestSearchCheck;
    }

    public boolean[] skillsTestPageOptionsCheck(String qnTitle, String noOfAnswers, String appendValue) {
        boolean[] skillsTestOptionsCheck = new boolean[39];

        skillsTestOptionsCheck[0] = visibilityCheck(getElement("MoreOptionsButton", qnTitle));
        getElement("MoreOptionsButton", qnTitle).click();

        String[] moreOptions = {"Edit", "View", "Duplicate", "Delete"};
        int optionPos = 1;
        for (String option : moreOptions) {
            skillsTestOptionsCheck[optionPos] = visibilityCheck(getElement("MoreButtonOptions", option));
            optionPos++;
        }
        System.arraycopy(skillsTestPageEditOptionCheck(qnTitle), 0, skillsTestOptionsCheck, 5, 22);

        System.arraycopy(skillsTestPageViewOptionCheck(qnTitle, noOfAnswers, appendValue), 0, skillsTestOptionsCheck, 27, 8);

        String editedQnTitle = qnTitle.concat("_Edited_");
        String duplicateQnTitle = editedQnTitle.concat(" (Copy)");
        skillsTestOptionsCheck[35] = skillsTestPageDuplicateOptionCheck(editedQnTitle, duplicateQnTitle);

        System.arraycopy(skillsTestPageDeleteOptionCheck(duplicateQnTitle), 0, skillsTestOptionsCheck, 36, 3);

        return skillsTestOptionsCheck;
    }

    public boolean[] skillsTestPageEditOptionCheck(String qnTitle) {

        boolean[] skillsTestEditOptionCheck = new boolean[22];
        String editAppendValue = "_Edited_";

        getElement("MoreButtonOptions", "Edit").jsClick();
        skillsTestEditOptionCheck[0] = visibilityCheck(getElement("EditConfirmationTopic"));
        skillsTestEditOptionCheck[1] = visibilityCheck(getElement("EditConfirmationMsg"));

        skillsTestEditOptionCheck[2] = visibilityCheck(getElement("CancelButton"));
        getElement("CancelButton").click();

        getElement("MoreOptionsButton", qnTitle).click();
        getElement("MoreButtonOptions", "Edit").click();

        skillsTestEditOptionCheck[3] = visibilityCheck(getElement("EditConfirmButton"));
        getElement("EditConfirmButton").click();

        getElement("SkillQnTitle").input(editAppendValue);
        skillsTestEditOptionCheck[4] = getElement("SkillQnTitle").getAttribute("value").contains(editAppendValue);

        getElement("SkillQnAnswerTimeMin").clear();
        String min = String.valueOf(generateRandomInteger(1, 10));
        getElement("SkillQnAnswerTimeMin").input(min);
        skillsTestEditOptionCheck[5] = String.valueOf(Integer.parseInt(getElement("SkillQnAnswerTimeMin").getAttribute("value"))).equals(min);

        getElement("SkillQnAnswerTimeSec").clear();
        String sec = String.valueOf(generateRandomInteger(1, 59));
        getElement("SkillQnAnswerTimeSec").input(sec);
        skillsTestEditOptionCheck[6] = String.valueOf(Integer.parseInt(getElement("SkillQnAnswerTimeSec").getAttribute("value"))).equals(sec);

        switchFrameToIndex(0);
        getElement("SkillQnAnsContent").sendKeys(editAppendValue);
        skillsTestEditOptionCheck[7] = (getElement("SkillQnAnsContentText").getText().contains(editAppendValue));
        driver.getDriver().switchTo().defaultContent();

        getElement("SkillsQnPageAnswersButton").click();

        int noOfAnswers = generateRandomInteger(2, 7);
        System.arraycopy(skillQnAnswerSelectionCheck(noOfAnswers), 0, skillsTestEditOptionCheck, 8, 3);

        String editAnsContentText = null;
        for (int ansFields = 1; ansFields <= noOfAnswers; ansFields++) {
            switchFrameToIndex(ansFields - 1);
            editAnsContentText = editAppendValue + ansFields;
            String qnDataID = getElement("SkillAnsContentIdentifierEdit").getAttribute("data-id");
            getElement("SkillQnAnsContentNoEditedText").sendKeys(editAnsContentText);

            skillsTestEditOptionCheck[11] = getElement("SkillAnsContent", qnDataID).getText().contains(editAnsContentText);
            driver.getDriver().switchTo().defaultContent();
        }

        skillsTestEditOptionCheck[12] = visibilityCheck(getElement("PreviewSkillQnButton"));
        getElement("PreviewSkillQnButton").jsClick();

        System.arraycopy(skillQnPreviewSectionCheck(noOfAnswers, editAppendValue, editAppendValue), 0, skillsTestEditOptionCheck, 13, 8);

        skillsTestEditOptionCheck[21] = visibilityCheck(getElement("SaveButton"));
        getElement("SaveButton").jsClick();

        getElement("ConfirmSaveButton").click();

        return skillsTestEditOptionCheck;
    }

    public boolean[] skillsTestPageViewOptionCheck(String qnTitle, String noOfAnswers, String appendValue) {

        getElement("MoreOptionsButton", qnTitle+"_Edited_").click();
        getElement("MoreButtonOptions", "View").jsClick();

        return skillQnPreviewSectionCheck(Integer.parseInt(noOfAnswers), appendValue, appendValue);
    }

    public boolean skillsTestPageDuplicateOptionCheck(String editedQnTitle, String dupQnTitle)
    {
        getElement("MoreOptionsButton", editedQnTitle).click();
        getElement("MoreButtonOptions", "Duplicate").jsClick();

        getElement("SkillQnTitle").click();

        driver.getDriver().switchTo().frame(getElement("DuplicateQnContentIframe"));
        getElement("SkillQnAnsContent").sendKeys("Duplicate");
        driver.getDriver().switchTo().defaultContent();

        getElement("CreateSkillQnButton").click();

        Awaitility.setDefaultTimeout(30000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(1500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(3000, MILLISECONDS);

        await().until(() -> {
            refreshPage();
            return (getElement("divElement", dupQnTitle).isDisplayed());
        });

        return visibilityCheck(getElement("divElement", dupQnTitle));
    }

    public boolean[] skillsTestPageDeleteOptionCheck(String duplicateQnTitle)
    {
        boolean[] skillsTestDeleteOptionCheck = new boolean[4];

        await().atMost(5000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("MoreOptionsButton",duplicateQnTitle)));
        getElement("MoreOptionsButton", duplicateQnTitle).click();

        getElement("MoreButtonOptions", "Delete").jsClick();

        skillsTestDeleteOptionCheck[0] = visibilityCheck(getElement("DeleteNoButton"));
        getElement("DeleteNoButton").click();

        getElement("MoreOptionsButton", duplicateQnTitle).click();
        getElement("MoreButtonOptions", "Delete").click();

        skillsTestDeleteOptionCheck[1] = visibilityCheck(getElement("DeleteYesButton"));
        getElement("DeleteYesButton").click();

        Awaitility.setDefaultTimeout(15000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(2000, MILLISECONDS);
        Awaitility.setDefaultPollDelay(500, MILLISECONDS);
        await().until(() -> {
            refreshPage();
            int visSize = getElements("divElementContains", duplicateQnTitle).size();
            return (visSize<2);
        });

        skillsTestDeleteOptionCheck[2] = (getElements("divElementContains", duplicateQnTitle).size()<2);
        return skillsTestDeleteOptionCheck;
    }

    public boolean[] skillsTestsSectionOpen(String qnTitle, String noOfAnswers, String appendValue)
    {
        boolean[] skillsTestSectionGeneralCheck = new boolean[30];

        await().atMost(5000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("SkillsTestsButton")));
        skillsTestSectionGeneralCheck[0] = visibilityCheck(getElement("SkillsTestsButton"));
        getElement("SkillsTestsButton").click();

        //single line awaitility
        await().atMost(5000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("SearchTestsTab")));

        skillsTestSectionGeneralCheck[1] = visibilityCheck(getElement("AddNewButton"));
        getElement("AddNewButton").jsClick();

        skillsTestSectionGeneralCheck[2] = visibilityCheck(getElement("CreateSkillsTestHeading"));

        String skillsTestName = "Skills_Test_" + ZonedDateTime.now();
        getElement("SkillsTestName").input(skillsTestName);
        skillsTestSectionGeneralCheck[3] = getElement("SkillsTestName").getAttribute("value").contains(skillsTestName);

        getElement("SkillsTestDescription").input(skillsTestName);
        skillsTestSectionGeneralCheck[4] = getElement("SkillsTestDescription").getAttribute("value").contains(skillsTestName);

        skillsTestSectionGeneralCheck[5] = visibilityCheck(getElement("AssignQuestionsMsg"));

        String qnSearchTitle = qnTitle+"_Edited_";
        skillsTestSectionGeneralCheck[6] = visibilityCheck(getElement("SkillQnPreviewButton",qnSearchTitle));
        getElement("SkillQnPreviewButton",qnSearchTitle).click();

        System.arraycopy(skillQnPreviewSectionCheck(Integer.parseInt(noOfAnswers), appendValue, appendValue), 0, skillsTestSectionGeneralCheck, 7, 8);

        skillsTestSectionGeneralCheck[15] = visibilityCheck(getElement("SkillQnAddButton", qnSearchTitle));
        getElement("SkillQnAddButton", qnSearchTitle).click();

        String firstAvailableQn = getElement("FirstSkillQnTitle").getText();
        getElement("SkillQnAddButton",firstAvailableQn).click();

        skillsTestSectionGeneralCheck[16] = visibilityCheck(getElement("SkillQnRemoveButton",firstAvailableQn));
        getElement("SkillQnRemoveButton",firstAvailableQn).click();

        skillsTestSectionGeneralCheck[17] = visibilityCheck(getElement("SkillQnRemovePopupTitle"));

        skillsTestSectionGeneralCheck[18] = visibilityCheck(getElement("SkillQnRemovePopupDescription"));

        skillsTestSectionGeneralCheck[19] = visibilityCheck(getElement("DeleteNoButton"));
        getElement("DeleteNoButton").click();

        getElement("SkillQnRemoveButton",firstAvailableQn).click();

        skillsTestSectionGeneralCheck[20] = visibilityCheck(getElement("SkillQnRemoveConfirmation"));
        getElement("SkillQnRemoveConfirmation").click();

        getElement("SkillQnAddButton",firstAvailableQn).click();

        List<Element> allSelectedQns = getElements("AllSelectedQns");

        skillsTestSectionGeneralCheck[21] = visibilityCheck(getElement("PreviewSkillsTestsButton"));
        getElement("PreviewSkillsTestsButton").jsClick();

        for(int qnNo = 0; qnNo < allSelectedQns.size() ; qnNo++)
        {
            getElement("SkillTestPreviewQnNo", String.valueOf(qnNo+1)).jsClick();
//            skillsTestSectionGeneralCheck[22] = visibilityCheck(getElement("PreviewQuestionContent", qnName[qnNo]));
            skillsTestSectionGeneralCheck[22] = visibilityCheck(getElement("PreviewQuestionContent"));
        }

        skillsTestSectionGeneralCheck[23] = visibilityCheck(getElement("ExitPreviewButton"));
        getElement("ExitPreviewButton").click();

        skillsTestSectionGeneralCheck[24] = visibilityCheck(getElement("CancelButton"));
        getElement("CancelButton").click();

        skillsTestSectionGeneralCheck[25] = visibilityCheck(getElement("CancelConfirmationTopic"));

        skillsTestSectionGeneralCheck[26] = visibilityCheck(getElement("CancelConfirmationDescription"));

        skillsTestSectionGeneralCheck[27] = visibilityCheck(getElement("CancelConfirmationReject"));

        skillsTestSectionGeneralCheck[28] = visibilityCheck(getElement("CancelConfirmationAccept"));

        getElement("CancelConfirmationReject").click();
        getElement("CreateSkillsTestsButton").click();

        skillsTestSectionGeneralCheck[29] = visibilityCheck(getElement("divElement", skillsTestName));

        Base json = new Base();
        json.updateInJson("SkillsTest", "TestName", skillsTestName);

        return skillsTestSectionGeneralCheck;
    }

    public void skillsTestDelete(String skillsTestName, String qnTitle)
    {
//        driver.setWebDriverWaitTime(30000);
        String editedQnTitle = qnTitle.concat("_Edited_");
        refreshPage();
        getElement("SkillsTestsButton").click();
//        await().atMost(1000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("SkillsTestMoreButton", skillsTestName)));
        await().atMost(10000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("SkillsTestsTableSkillName")));
        getElement("SkillsTestMoreButton", skillsTestName).click();
        getElement("MoreButtonOptions", "Delete").jsClick();
        getElement("SkillsQnsDelete").click();

        refreshPage();
//        getElement("QnLibrarySection").click();
        await().atMost(10000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("SkillsQnsTableQuestionTitle")));
        await().atMost(10000, MILLISECONDS).untilAsserted(() -> visibilityCheck(getElement("MoreOptionsButton", editedQnTitle)));
        getElement("MoreOptionsButton", editedQnTitle).jsClick();
        getElement("MoreButtonOptions", "Delete").jsClick();
        getElement("DeleteYesButton").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

}
