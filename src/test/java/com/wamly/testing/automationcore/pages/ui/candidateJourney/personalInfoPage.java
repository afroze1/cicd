package com.wamly.testing.automationcore.pages.ui.candidateJourney;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class personalInfoPage extends Page {
    public personalInfoPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void personalInfoFormFill()
    {
        clearInputText(getElement("CandidateName"),getAppendedValues(5,true,false));
        clearInputText(getElement("CandidateSurname"),getAppendedValues(5,true,false));
        clearInputText(getElement("CandidateEmail"),System.getProperty("test.candidate.username"));
        clearInputText(getElement("CandidateMobileNumber"),generateRandomString(10,false,true));
        allTextFieldsFill();
        allDatesRandomSelect();
        allDropdownRandomSelect();
    }

    public void allTextFieldsFill()
    {
        elementListFill("TextArea",15, true, false, true);
        elementListFill("NumberInput",4, false, true, false);
        elementListFill("AllTextField",5, true, false, true);

        //Fill all 'Numbers only' Field
        List<Element> numbersOnlyFieldElements = getElements("NumbersOnly");
        for (Element numberOnlyElement : numbersOnlyFieldElements) {
            clearInputText(numberOnlyElement, generateRandomString(10, false, true));
        }

        //Fill all 'Email only' Field
        List<Element> emailOnlyFieldElements = getElements("EmailOnly");
        for (Element emailOnlyElement : emailOnlyFieldElements) {
            clearInputText(emailOnlyElement,getAppendedValues(2,true,true)+"@test.com");
        }

        //Select Address from Address Dropdown
        String allFields = uiFieldsFetch();
        if(allFields.contains("Address"))
        {
            clearInputText(getElement("CandidateAddress"),getAppendedValues(1,false,true));
            getElement("FirstAddressOption").click();
        }
    }

    public void elementListFill(String listElementName, int limit, boolean useLetters, boolean useNumbers, boolean append)
    {
        List<Element> allFieldElements = getElements(listElementName);
        for (Element fieldElement : allFieldElements) {
            if(fieldElement.getAttribute("value").isEmpty())
            {
                if(append)
                {
                    fieldElement.input(getAppendedValues(limit, useLetters, useNumbers));
                }
                else {
                    fieldElement.input(generateRandomString(limit, useLetters, useNumbers));
                }
            }
        }
    }

    public ZonedDateTime nextPage()
    {
        getElement("NextButton").click();
        return (ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Z")));
    }

    public void allDropdownRandomSelect()
    {
        List<Element> dropDownElements = getElements("AllDropdown");
        for (Element drpDwnElement : dropDownElements) {
            randomDropdownSelect(drpDwnElement);
        }

        List<Element> checkboxElements = getElements("CheckboxDropdown");
        for (Element checkBoxElement : checkboxElements) {
            randomDropdownSelect(checkBoxElement);
            getElement("closeClick").click();
        }
    }

    public void allDatesRandomSelect()
    {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("MM/DD/YYYY");
        List<Element> dateElements = getElements("AllDateSelector");
        for (Element dateElement : dateElements) {
            Random random = new Random();
            int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
            int maxDay = (int) LocalDate.of(2005, 1, 1).toEpochDay();
            long randomDay = minDay + random.nextInt(maxDay - minDay);
            LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
            dateElement.click();
            dateElement.input(randomBirthDate.format(formatters));
        }
    }

    public boolean VisibilityCheck(String ElementName)
    {
        return (getElement(ElementName).isDisplayed());
    }

    public void randomDropdownSelect(WebElement dropDownElement)
    {
        dropDownElement.click();
        getElement("CandidateDropdownFirstOption").click();
    }

    public void clearInputText(Element ElementName, String inputValue)
    {
        driver.setWebDriverWaitTime(15000);
        ElementName.clear();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        ElementName.input(inputValue);
    }

    public String getAppendedValues(int limit, boolean useLetters, boolean useNumbers)
    {
       return ("Test"+generateRandomString(limit,useLetters,useNumbers));
    }

    public String generateRandomString(int limit, boolean useLetters, boolean useNumbers)
    {
        return RandomStringUtils.random(limit, useLetters, useNumbers);
    }

    public String uiFieldsFetch()
    {
        List<Element> AllFields = getElements("UIFieldValues");
        String[] responseArray = new String[AllFields.size()];
        int loc=0;
        for (Element field : AllFields) {
            responseArray[loc] = field.getText();
            loc++;
        }
        return Arrays.toString(responseArray);
    }
}
