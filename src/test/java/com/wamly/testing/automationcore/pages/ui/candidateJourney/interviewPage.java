package com.wamly.testing.automationcore.pages.ui.candidateJourney;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.pages.Base;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.apache.commons.lang.RandomStringUtils;
import org.awaitility.Awaitility;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Random;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class interviewPage extends Page {

    public interviewPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public boolean VisibilityCheck(String ElementName)
    {
        return (getElement(ElementName).isDisplayed());
    }

    public void preInterviewPage()
    {
        getElement("CommenceInterview").click();
        getElement("CommencePracticeInterviewConfirmation").click();
    }

    public void commencePracticeInterview()
    {
        getElement("CommencePracticeInterview").click();
    }

    public String interfaceTour()
    {
        String interfaceTourStepTopics = getElement("InterfaceTourNextStepTopic").getText();
        getElement("InterfaceTourNextStep").click();
        return interfaceTourStepTopics;
    }

    public String practiceInterview()
    {
        getElement("PracticeInterviewSampleQuestionTry").click();
        String uiPracticeElements = "";
        uiPracticeElements = getValueClick(uiPracticeElements,"PracticeInterviewAnswerTime");
        uiPracticeElements = getValueClick(uiPracticeElements,"PracticeInterviewStartRecording");
        //getElement("BlurScreenButton").click();
        uiPracticeElements = uiPracticeElements + "," +getElement("BlurScreenLabel").getText();
        uiPracticeElements = getValueClick(uiPracticeElements,"PracticeInterviewNextQuestion");
        return uiPracticeElements;
    }

    public String getValueClick(String previousElement, String uiElement)
    {
        String elementList = previousElement + "," + getElement(uiElement).getText();
        getElement(uiElement).click();
        return elementList;
    }

    public void commenceInterview()
    {
        getElement("CommenceInterview").click();
    }

    public String attendInterview(String[] interviewQuestionList)
    {
        Awaitility.setDefaultTimeout(15000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(500, MILLISECONDS);
        StringBuilder uiQuestion = new StringBuilder();
        int interviewLimit = interviewQuestionList.length;
        for(int noIntrvwQns=0; noIntrvwQns < interviewLimit; noIntrvwQns++)
        {
            driver.setWebDriverWaitTime(30000);
            getElement("CommenceRecording").click();

            //InterviewTimer
            uiQuestion.append(",").append(getElement("InterviewQuestion").getText());
            getElement("BlurScreenButton").click();
            //PracticeInterviewAnswerTime
            if(noIntrvwQns == (interviewLimit-1))
            {
                getElement("InterviewQuestionFinishButton").click();
                Base json = new Base();
                json.updateInJson("TimeStamp","DoneStamp", String.valueOf(ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Z"))));
            }
            else
            {
                getElement("InterviewNextQuestion").click();
                //TakeABreather
                getElement("TakeABreatherNext").click();
            }
        }
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return uiQuestion.toString();
    }

    public void interviewFeedback()
    {
        String[] reviewOptions = {"LovedItReview", "CouldBeBetterReview"};
        Random r=new Random();
        String fB = reviewOptions[r.nextInt(reviewOptions.length)];
        getElement(fB).click();
        if(fB.equals("CouldBeBetterReview"))
        {
            getElement("ReviewText").input("Test Feedback " + RandomStringUtils.random(15, true, true));
            getElement("ReviewSendButton").click();
        }
    }

    public void dashboardLink()
    {
        getElement("DashboardLink").click();
    }
}
