package com.wamly.testing.automationcore.pages.ui.candidateJourney;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;


public class preInterviewCheck extends Page {
    public preInterviewCheck(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void proceedToPreInterviewTests() {
        driver.setWebDriverWaitTime(15000);
        getElement("CommencePreInterviewTests").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public boolean VisibilityCheck(String ElementName)
    {
        return (getElement(ElementName).isDisplayed());
    }

    public void preInterviewCheckProceed()
    {
        driver.setWebDriverWaitTime(15000);
        getElement("ProceedToPreInterviewButton").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public void proceedToHardwareTest()
    {
        driver.setWebDriverWaitTime(15000);
        getElement("ProceedToHardwareTestButton").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public void cameraMicrophoneTest() {
        getElement("RecordingTest").click();
        getElement("RecordingTestCommence").click();

        driver.setWebDriverWaitTime(12000);
        getElement("RecordingPlayback").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        getElement("ConfirmCheckbox").click();
    }

    public void nextPage()
    {
        getElement("NextButton").click();
    }
}
