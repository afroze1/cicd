package com.wamly.testing.automationcore.pages.ui.candidateJourney;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.interactions.Actions;

public class interviewLandingPage extends Page {
    public interviewLandingPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void explainerVideoOpen() {
        getElement("VideoButton").click();
        driver.getDriver().switchTo().frame(getElement("VideoIframe"));
    }

    public void explainerVideoPlayClose() {
        getElement("VideoPlayButton").click();
        Actions action = new Actions(driver.getDriver());
        action.moveToElement(getElement("VideoScreen")).perform();
        action.click().build().perform();
        driver.getDriver().switchTo().defaultContent();
        action.doubleClick(getElement("NextButton")).perform();
    }
    public boolean videoVisibilityCheck(String ElementName)
    {
        return (getElement(ElementName).isEnabled());
    }

    public void navigateInterviewLandingPage() {
        getElement("EmailAddress").input(System.getProperty("test.candidate.username"));
        getElement("ConsentCheckbox").click();
    }

    public boolean VisibilityCheck(String ElementName)
    {
        return (getElement(ElementName).isDisplayed());
    }

    public String ValidationCheck(String ElementName)
    {
        return (getElement(ElementName).getText().trim());
    }

    public void nextPage()
    {
        driver.setWebDriverWaitTime(20000);
        getElement("NextButton").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public void otpPaste(String otp) {
        getElement("EnterOTP").input(otp);

    }

    public void wrongOtpPaste() {
        getElement("EnterOTP").input(RandomStringUtils.random(3, false, true));
        getElement("NextButton").click();
    }

    public void introVideo()
    {
        getElement("IntroVideo").click();
        getElement("ProceedButton").click();
    }
}
