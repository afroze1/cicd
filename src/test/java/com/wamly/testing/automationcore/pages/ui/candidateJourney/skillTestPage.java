package com.wamly.testing.automationcore.pages.ui.candidateJourney;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.pages.Base;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import org.awaitility.Awaitility;
import org.openqa.selenium.*;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;

public class skillTestPage extends Page {

    public skillTestPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public boolean VisibilityCheck(String ElementName)
    {
        try
        {
            return (getElement(ElementName).isDisplayed());
        } catch (TimeoutException e) {
            getElement(ElementName).sendKeys(Keys.F5);
            return (getElement(ElementName).isDisplayed());
        }
    }

    public void commenceSkillTest() {
        driver.setWebDriverWaitTime(20000);
        try
        {
            getElement("CommenceSkillTest").click();
        } catch (StaleElementReferenceException exc) {
            getElement("CommenceSkillTest").sendKeys(Keys.F5);
            getElement("CommenceSkillTest").click();
        }
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public String attendSkillTest(String[] questions, String[] correctAnswers) {
        Awaitility.setDefaultTimeout(5000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(500, MILLISECONDS);

        String uiQuestionList = null;
        int noSkillTestQns = questions.length;
        String onScreenQuestion;
        int qc, qcFinal;
        for(int aqn=0; aqn < noSkillTestQns ; aqn++)
        {
            qcFinal = 0;
            onScreenQuestion = getElement("OnScreenQuestion").getText();
            uiQuestionList = uiQuestionList + "," + onScreenQuestion;
            for(qc=0 ; qc < noSkillTestQns ; qc++)
            {
                if(onScreenQuestion.equals(questions[qc]))
                {
                    int correctAnswerOption = 0, answerOptionsCount = 0;
                    List<Element> answerOptions = getElements("OnScreenAnswers");
                    for (Element answerElement : answerOptions) {
                        if(answerElement.getText().equals(correctAnswers[qc]))
                        {
                            qcFinal=qc;
                            correctAnswerOption = answerOptionsCount;
                        }
                        answerOptionsCount++;
                    }
                    answerOptions.get(correctAnswerOption).click();
                    String previousSkillQuestion = questions[qcFinal];
                    getElement("SaveAndContinue").click();
                    if(qc<noSkillTestQns-1)
                    {
                        await().until(() -> {
                            String finalOnScreenQuestion = getElement("OnScreenQuestion").getText();
                            return (!finalOnScreenQuestion.equals(previousSkillQuestion));
                        });
                    }
                }
            }
        }
        return uiQuestionList;
    }

    public String failSkillTest(String[] questions, String[] correctAnswers)
    {
        Awaitility.setDefaultTimeout(5000, MILLISECONDS);
        Awaitility.setDefaultPollInterval(500, MILLISECONDS);
        Awaitility.setDefaultPollDelay(500, MILLISECONDS);

        String uiQuestionList = null;
        int noSkillTestQns = questions.length;
        String onScreenQuestion;
        int qc, qcFinal;
        for(int aqn=0; aqn < noSkillTestQns ; aqn++)
        {
            qcFinal = 0;
            onScreenQuestion = getElement("OnScreenQuestion").getText();
            uiQuestionList = uiQuestionList + "," + onScreenQuestion;
            for(qc=0 ; qc < noSkillTestQns ; qc++)
            {
                if(onScreenQuestion.equals(questions[qc]))
                {
                    List<Element> answerOptions = getElements("OnScreenAnswers");
                    if((answerOptions.get(1).getText()).equals(correctAnswers[qc]))
                    {
                        qcFinal=qc;
                        answerOptions.get(2).click();
                    }
                    else {
                        qcFinal=qc;
                        answerOptions.get(1).click();
                    }
                    getElement("SaveAndContinue").click();
                }
                String previousSkillQuestion = questions[qcFinal];
                if(qc<noSkillTestQns-1)
                {
                    await().until(() -> {
                        String finalOnScreenQuestion = getElement("OnScreenQuestion").getText();
                        return (!finalOnScreenQuestion.equals(previousSkillQuestion));
                    });
                }
            }
        }
        Base json = new Base();
        json.updateInJson("TimeStamp","DoneStamp", String.valueOf(ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Z"))));
        return uiQuestionList;
    }

    public void dashboardPage()
    {
        driver.setWebDriverWaitTime(30000);
        getElement("FirstInterviewDetails").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public void findProjectDropdown(String projectName)
    {
        driver.setWebDriverWaitTime(30000);
        getElement("projectStatusXpath",projectName).click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
    }

    public String projectNameVerify(String value)
    {
        return getElement("projectNameXpath",value).getText().trim();
    }

    public String projectStatusVerify(String value)
    {
        return getElement("projectStatusXpath",value).getText().trim();
    }

    public void backToDashboard()
    {
        getElement("BackToDashboardButton").click();
    }
}
