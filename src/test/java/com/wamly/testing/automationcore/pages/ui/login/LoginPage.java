package com.wamly.testing.automationcore.pages.ui.login;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.browser.page.Page;
import com.wamly.testing.automationcore.utils.Decryption;
import org.apache.commons.lang.RandomStringUtils;

import java.util.Base64;
import java.util.List;


public class LoginPage extends Page {

    public LoginPage(ParentDriver driver) {
        super(driver);
        setLocatorCollection();
    }

    public void performLogin() {
        driver.setWebDriverWaitTime(30000);
        inputUsernamePassword();
        getElement("loginButton").click();
        getElement("loginSkipMFA").click();
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);


     //fetch login accesstoken from application local storage
//        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver.getDriver();
//        List<String> localStorageKeys = (List<String>) jsExecutor.executeScript("var keys = Object.keys(localStorage); return keys;");
//        String tokenKey = getLocalStorageKeyName(localStorageKeys,"idToken");
//        String tokenValue = String.valueOf(jsExecutor.executeScript("return window.localStorage.getItem('" + tokenKey + "');"));
//
//        Base base = new Base();
//        base.updateInJson("API","idToken",tokenValue);
    }

    public String passwordSort(String passwordStatus) throws Exception {
        String newErrorMessages="", passwordError="";
        switch(passwordStatus)
        {
            case "blank":
                inputPasswordString("");
                newErrorMessages = getElement("enterValuesErrorMessage").getText() + "%";
                passwordError = getLoginJsonValue("BlankPassword");
                break;
            case "unregistered":
                inputPasswordString(RandomStringUtils.random(2, true, true)+Decryption.decrypt(System.getProperty("test.password"),System.getProperty("test.securityKey")));
                newErrorMessages = getElement("incorrectValuesErrorMessage").getText() + "%";
                passwordError = getLoginJsonValue("UnregisteredPassword");
                break;
            case "invalid":
                inputPasswordString(RandomStringUtils.random(10, true, true));
                newErrorMessages = getElement("incorrectValuesErrorMessage").getText() + "%";
                passwordError = getLoginJsonValue("InvalidPassword");
                break;
            case "valid":
            default:
                inputPasswordString(Decryption.decrypt(System.getProperty("test.password"),System.getProperty("test.securityKey")));
                break;
        }
        return newErrorMessages+passwordError;
    }

    public String loginOperations(String usernameStatus, String passwordStatus) throws Exception {
        String errorMessages = "", newErrorMessages = "", usernameError="";

        switch(usernameStatus)
        {
            case "blank":
                inputUsernameString("");
                errorMessages = getElement("enterEmailErrorMessage").getText() + "%";
                usernameError = getLoginJsonValue("BlankUsername");

                break;
            case "unregistered":
                inputUsernameString(RandomStringUtils.random(2, true, true)+Decryption.decrypt(System.getProperty("test.username"),System.getProperty("test.securityKey")));
                errorMessages = getElement("incorrectEmailErrorMessage").getText() + "%";
                usernameError = getLoginJsonValue("UnregisteredUsername");
                break;
            case "invalid":
                inputUsernameString(RandomStringUtils.random(10, true, true));
                errorMessages = getElement("invalidEmailErrorMessage").getText() + "%";
                usernameError = getLoginJsonValue("InvalidUsername");
                break;
            case "valid":
            default:
                inputUsernameString(Decryption.decrypt(System.getProperty("test.username"),System.getProperty("test.securityKey")));
                newErrorMessages = passwordSort(passwordStatus);
                break;
        }
        if(!newErrorMessages.isEmpty())
        {
            return newErrorMessages;
        }
        else{
            return errorMessages+usernameError;
        }
    }

    public void inputUsernameString(String username)
    {
        getElement("loginEmail").input(username);
        getElement("loginNextButton").click();
    }

    public void inputPasswordString(String password)
    {
        getElement("loginPassword").input(password);
        getElement("loginButton").click();
    }

    public void inputUsernamePassword()
    {
        getElement("loginEmail").input(Decryption.decrypt(System.getProperty("test.username"),System.getProperty("test.securityKey")));
        getElement("loginNextButton").click();
        getElement("loginPassword").input(Decryption.decrypt(System.getProperty("test.password"),System.getProperty("test.securityKey")));
    }

    public String showHideButtonInteract()
    {
        inputUsernamePassword();
        getElement("showHideButton").click();
        return getElement("loginPassword").getAttribute("value");
    }

    public String getDecodePwd(String strPwd) throws  Exception {
        byte[] decodedBytes = Base64.getDecoder().decode(strPwd);
        return new String(decodedBytes);
    }

    public String getLocalStorageKeyName(List<String> Keys, String keyText)
    {
        String keyNameValue=null;
        for (String keyName : Keys) {
            if (keyName.contains(keyText)) {
                keyNameValue=keyName;
            }
        }
        return keyNameValue;
    }
}
