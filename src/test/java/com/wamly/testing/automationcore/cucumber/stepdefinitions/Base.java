package com.wamly.testing.automationcore.cucumber.stepdefinitions;

import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.data.TestData;
import com.wamly.testing.automationcore.data.TestDataCollection;
import com.wamly.testing.automationcore.utils.JSON;
import com.wamly.testing.automationcore.utils.TestLog;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.internal.collections.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Base {

    protected TestDataCollection testdataCollection;

    String fileName1 = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"testdata"+File.separator+"runTimeData"+File.separator+"testdata.json";
    String fileName2 = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"endPoints"+File.separator+"apiEndPoints.json";
    String fileName3 = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"apiData"+File.separator+"payload"+File.separator;

    protected void setTestDataCollection(String section, String name) {
        String dataFile = System.getProperty("test.testdata.path")+ File.separator + section + File.separator + name.toLowerCase() + ".json";
        if (this.getClass().getResourceAsStream(dataFile) == null) {
            TestLog.log().fatal("Failed to load testdata file "+File.separator + section + File.separator + name.toLowerCase() + ".json" +", please verify testdata file against the class {}", this.getClass());
            throw new FrameworkException("Failed to load testdata file "+File.separator + section + File.separator + name.toLowerCase() + ".json" +", please verify testdata file against the class");
        }
        testdataCollection = JSON.loadJSONAsObject(TestDataCollection.class, this.getClass().getResourceAsStream(dataFile));
    }

    protected String getData(String value) {
        TestData elementLocator = testdataCollection.getTestdata(value);
        return elementLocator.testDataValue;
    }

    public String readJsonFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }

    public String getDataFromJson(String file, String module,String key) {
        String fileName = "";
        try {

            if(file.contains("runData"))
            {
                fileName = fileName1;
            }
            else
            {
                fileName = fileName2;
            }

            // Read the JSON content
            String jsonContent = readJsonFile(fileName);

            // Parse the JSON content into a JSONObject
            org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);

            // Retrieve the value
            if (jsonObject.has(module) && !jsonObject.get(module).toString().equals("{}")) {
                return jsonObject.getJSONObject(module).getString(key);
            }else{
                throw new FrameworkException(module+" - "+key +" is not updated in the json file");
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw new FrameworkException(module+" - "+key +" is not updated in the json file");
        }
    }

    public String[] jsonRead(String formName, String section, String fieldName) throws IOException {
        // Read the JSON content
        String jsonContent = readJsonFile(fileName3 + formName + ".json");

        // Parse the JSON content into a JSONObject
        org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);

        JSONArray jsonArray = jsonObject.getJSONArray(section);
        JSONObject fields = null;
        String[] fieldValues = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            fields = jsonArray.getJSONObject(i);
            fieldValues[i]=fields.getString(fieldName);
        }
        return fieldValues;
    }

    public String jsonConditionCheckReturnValue(String formName, String section, String conditionCheckField, boolean conditionValue, String responseField) throws IOException {
        String jsonContent = readJsonFile(fileName3 + formName + ".json");
        String correctAnswer = null;
        org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);

        JSONArray jsonArray = jsonObject.getJSONArray(section);
        JSONObject fields = null;
        for (int i = 0; i < jsonArray.length(); i++) {
            fields = jsonArray.getJSONObject(i);
            if(fields.get(conditionCheckField).equals(conditionValue))
            {
                correctAnswer = (fields.get(responseField)).toString();
            }
        }
        return correctAnswer;
    }

    public String getSimpleJsonValue(String formName, String fieldName) throws IOException {
        String jsonContent = readJsonFile(fileName3 + formName + ".json");

        // Parse the JSON content into a JSONObject
        org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);
        return jsonObject.getString(fieldName);
    }

    public String[] skillTestFetch(String returnValue) throws IOException {
        int noTotalQns = (jsonRead("skillTest","questions","id")).length;
        String skillQuestion, SkillQuestionCorrectAnswers;
        String[] allSkillQuestions = new String[noTotalQns];
        String[] allSkillQuestionCorrectAnswers = new String[noTotalQns];
        String allSkillQuestionAllAnswers = null;
        String[] skillQuestionAllAnswers = new String[noTotalQns];

        for(int sqn=0; sqn < noTotalQns; sqn++)
        {
            skillQuestion = getSimpleJsonValue("skillTestQuestion"+(sqn+1), "value");
            SkillQuestionCorrectAnswers = jsonConditionCheckReturnValue("skillTestQuestion"+(sqn+1), "options", "correct", true,"value");
            allSkillQuestions[sqn] = skillQuestion.replaceAll("<p>","").replaceAll("</p>","");
            allSkillQuestionCorrectAnswers[sqn] = SkillQuestionCorrectAnswers.replaceAll("<p>","").replaceAll("</p>","");
            allSkillQuestionAllAnswers = Arrays.toString(jsonRead("skillTestQuestion"+(sqn+1),"options","value"));
            skillQuestionAllAnswers[sqn] = allSkillQuestionAllAnswers.replaceAll("<p>","").replaceAll("</p>","");
        }
        switch(returnValue) {
            case "Questions":
                return allSkillQuestions;
            case "Correct Answers":
                return allSkillQuestionCorrectAnswers;
            case "All Answers":
                return skillQuestionAllAnswers;
            default:
                return null;
        }
    }
}
