package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui.login;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.ui.login.LoginPage;
import com.wamly.testing.automationcore.utils.Decryption;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LoginDefinitions extends Base {

    private CukesWorld cukesWorld;
    LoginPage login;
    public LoginDefinitions()
    {
        this.cukesWorld= RunnerCucumberTest.cukesWorld.get();
        setTestDataCollection("login","loginpage");
    }

    @Given("User launches the application url")
    public void launchUrl()
    {
        cukesWorld.getDriver().navigate(System.getProperty("test.base.url"));
    }

    @Given("User attempts login using {string} and {string}")
    public void userAttemptsLoginUsingAnd(String usernameStatus, String passwordStatus) throws Exception {
        login = new LoginPage(cukesWorld.getDriver());
        String errorMessages = login.loginOperations(usernameStatus, passwordStatus);
        if(!errorMessages.isEmpty())
        {
            cukesWorld.getSoftAssert().assertTrue((errorMessages.split("%")[0].equals(errorMessages.split("%")[1])),"Appearance verification of '" +errorMessages.split("%")[1]+ "' error message");
        }
        cukesWorld.getSoftAssert().assertAll();
    }

    @When("User Login to the wamly application with valid credentials")
    public void checkLogin() throws Exception
    {
        login = new LoginPage(cukesWorld.getDriver());
        login.performLogin();
    }

    @Then("User should see the home page title")
    public void verifyTitle()
    {
        String titleString = cukesWorld.getDriver().getTitle();
        cukesWorld.getSoftAssert().assertEquals(titleString,getData("CorrectTitle"),String.format("Appearance verification of '%s", getData("CorrectTitle"))+"' as the Window Title");
        cukesWorld.getSoftAssert().assertAll();
    }


    @Then("User should be able to interact with the SHOW-HIDE button")
    public void userShouldBeAbleToInteractWithTheSHOWHIDEButton() {
        login = new LoginPage(cukesWorld.getDriver());
        String uiPassword = login.showHideButtonInteract();
        String password = Decryption.decrypt(System.getProperty("test.password"),System.getProperty("test.securityKey"));
        cukesWorld.getSoftAssert().assertEquals(uiPassword,password,"Verification of Password SHOW-HIDE button functionality");
        cukesWorld.getSoftAssert().assertAll();
    }
}
