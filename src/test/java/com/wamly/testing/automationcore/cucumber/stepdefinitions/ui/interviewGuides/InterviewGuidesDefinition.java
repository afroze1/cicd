package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui.interviewGuides;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.ui.candidateJourney.interviewLandingPage;
import com.wamly.testing.automationcore.pages.ui.interviewGuides.interviewGuidesPage;
import com.wamly.testing.automationcore.pages.ui.projectCreation.formCreationPage;
import com.wamly.testing.automationcore.selenium.Element;
import io.cucumber.java.en.And;

import java.io.IOException;

public class InterviewGuidesDefinition extends Base {
    private CukesWorld cukesWorld;
    interviewGuidesPage interviewGuides;

    String InterviewGuideName;

    public InterviewGuidesDefinition() {
        this.cukesWorld = RunnerCucumberTest.cukesWorld.get();
    }



    @And("User checks the Interview Guide options")
    public void userChecksTheInterviewGuideOptions() throws IOException {
        interviewGuides = new interviewGuidesPage(cukesWorld.getDriver());
        interviewGuides.navigatetoInterviewGuides();
        String[] uimoreOptions = interviewGuides.interviewGuidesOptions();
        String[] moreOptions = {"Duplicate", "Edit", "Deactivate", "Delete"};
        int optionPos=0;
        for(String option: moreOptions)
        {
            cukesWorld.getSoftAssert().assertEquals(uimoreOptions[optionPos],option,"Appearance verification of '" + option + "' option in the 'More' button");
            boolean[] visibilityResponse = interviewGuides.interviewGuideActions(option);
            String interviewGuidesValue = interviewGuides.getInterviewGuidesJsonValue(option);
            if(interviewGuidesValue.contains(","))
            {
                for(int i=0; i < visibilityResponse.length ; i++)
                {
                    cukesWorld.getSoftAssert().assertTrue(visibilityResponse[i], "Appearance verification of "+interviewGuides.getInterviewGuidesJsonValue(interviewGuidesValue.split(",")[i]));
                }
            }
            else {
                cukesWorld.getSoftAssert().assertTrue(visibilityResponse[0], "Appearance verification of "+interviewGuides.getInterviewGuidesJsonValue(interviewGuidesValue));
            }
            optionPos++;
        }

    }

    @And("User creates the Interview Guides")
    public void userCreatesAInterviewGuide() throws IOException {

        interviewGuides = new interviewGuidesPage(cukesWorld.getDriver());
        boolean[] createVisibilityResponse = interviewGuides.interviewGuideCreation();
        String interviewGuidesValue = interviewGuides.getInterviewGuidesJsonValue("Create");
        for(int i=0; i < createVisibilityResponse.length ; i++)
        {
            cukesWorld.getSoftAssert().assertTrue(createVisibilityResponse[i], "Appearance verification of "+interviewGuides.getInterviewGuidesJsonValue(interviewGuidesValue.split(",")[i]));
        }

    }

}




