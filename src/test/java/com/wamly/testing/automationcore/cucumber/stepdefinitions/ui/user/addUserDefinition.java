package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui.user;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.api.API_E2E;
import com.wamly.testing.automationcore.pages.ui.interviewGuides.interviewGuidesPage;
import com.wamly.testing.automationcore.pages.ui.user.addUserPage;
import io.cucumber.java.en.And;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class addUserDefinition extends Base {
    private CukesWorld cukesWorld;
    addUserPage user;
    public addUserDefinition()
    {
        this.cukesWorld = RunnerCucumberTest.cukesWorld.get();
    }

    @And("User creates a new user")
    public void createUser() throws IOException {

        user = new addUserPage(cukesWorld.getDriver());
        boolean[] visibilityResponse = user.createANewUser();
//        String userValue = user.getUserJsonValue("Create");
//        if(userValue.contains(","))
//        {
//            for(int i=0; i < visibilityResponse.length ; i++)
//            {
//                cukesWorld.getSoftAssert().assertTrue(visibilityResponse[i], "Appearance verification of "+user.getUserJsonValue(userValue.split(",")[i]));
//            }
//        }
//        else {
//            cukesWorld.getSoftAssert().assertTrue(visibilityResponse[0], "Appearance verification of "+user.getUserJsonValue(userValue));
//        }



    }
    @And("User fetches confirmation email")
    public void fetchEmail() {

        user = new addUserPage(cukesWorld.getDriver());
        API_E2E fetchEmail1=new API_E2E();
        ZonedDateTime otpMailsentTime = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Z"));

        String[] values =  fetchEmail1.getEmail(otpMailsentTime,System.getProperty("mailsac.baseUrl"), System.getProperty("mailsac.endUrl"));
        System.out.println(values[0]);
        System.out.println(values[1]);
        System.out.println(values[2]);
        String check1 = getDataFromJson("runData","addUser","FirstName");
        System.out.print("check1"+check1);


    }
}
