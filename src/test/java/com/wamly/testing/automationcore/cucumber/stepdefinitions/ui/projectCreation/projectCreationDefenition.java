package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui.projectCreation;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.ui.projectCreation.formCreationPage;
import com.wamly.testing.automationcore.pages.ui.projectCreation.skillsTestCreationPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class projectCreationDefenition extends Base {

    private CukesWorld cukesWorld;
    formCreationPage forms;
    skillsTestCreationPage skillsTest;
    public projectCreationDefenition() {
        this.cukesWorld= RunnerCucumberTest.cukesWorld.get();
    }

    @Given("User checks the forms options")
    public void userChecksTheFormsOptions() throws IOException {

        forms = new formCreationPage(cukesWorld.getDriver());
        forms.formsPageOpen();
        boolean videoVisibility = forms.formsTrainingVideo();
        cukesWorld.getSoftAssert().assertTrue(videoVisibility,"Appearance verification of Forms Training Video");

        String[] uiMoreOptions = forms.formsOptions();
        String[] moreOptions = {"Set As Default", "Duplicate", "Edit", "Deactivate", "Delete"};
        int optionPos=0;
        for(String option: moreOptions)
        {
            cukesWorld.getSoftAssert().assertEquals(uiMoreOptions[optionPos],option,"Appearance verification of '" + option + "' option in the 'More' button");
            boolean[] visibilityResponse = forms.formsActions(option);
            String formValue = forms.getFormsJsonValue(option);
            if(formValue.contains(","))
            {
                for(int i=0; i < visibilityResponse.length ; i++)
                {
                    cukesWorld.getSoftAssert().assertTrue(visibilityResponse[i], "Appearance verification of "+forms.getFormsJsonValue(formValue.split(",")[i]));
                }
            }
            else {
                cukesWorld.getSoftAssert().assertTrue(visibilityResponse[0], "Appearance verification of "+forms.getFormsJsonValue(formValue));
            }
            optionPos++;
        }
    }

    @Then("User checks the forms labels")
    public void userChecksTheFormsLabels() throws IOException {
        forms = new formCreationPage(cukesWorld.getDriver());
        boolean[] createVisibilityResponse = forms.formCreation();
        String formValue = forms.getFormsJsonValue("Create");
        for(int i=0; i < createVisibilityResponse.length ; i++)
        {
            cukesWorld.getSoftAssert().assertTrue(createVisibilityResponse[i], "Appearance verification of "+forms.getFormsJsonValue(formValue.split(",")[i]));
        }

        boolean[] labelsToolsResponse = forms.formLabelsToolsCheck();
        String formLabelValue = forms.getFormsJsonValue("AllLabelOptions");
        for(int i=0; i < 4 ; i++)
        {
            cukesWorld.getSoftAssert().assertTrue(labelsToolsResponse[i], "Appearance verification of '"+formLabelValue.split(",")[i]+"' label");
        }
        for(int i=4; i < labelsToolsResponse.length ; i++)
        {
            cukesWorld.getSoftAssert().assertTrue(labelsToolsResponse[i], "Appearance verification of '"+formLabelValue.split(",")[i]+"' label in the ToolBox section");
        }
    }

    @Then("User creates a form")
    public void userCreatesAForm() throws IOException {
        forms = new formCreationPage(cukesWorld.getDriver());
        forms.addAllFields();
        boolean formCreated = forms.createForm();
        cukesWorld.getSoftAssert().assertTrue(formCreated, "Appearance verification of "+forms.getFormsJsonValue("UpdateSuccessMessage"));

    }

    @And("User creates the skill test questions")
    public void userCreatesTheSkillTestQuestions() throws IOException {
        skillsTest = new skillsTestCreationPage(cukesWorld.getDriver());
        skillsTest.skillsTestPageOpen();

        boolean videoVisibility = skillsTest.skillsTestTrainingVideo();
        cukesWorld.getSoftAssert().assertTrue(videoVisibility,"Appearance verification of Skills Test Training Video");

        boolean isInitialPage = skillsTest.skillsTestIsInitialPage();
        if(isInitialPage)
        {
            boolean[] skillsTestInitialCheck = skillsTest.skillsTestInitialPageCheck();
            String skillsTestInitValue = skillsTest.getSkillsTestJsonValue("SkillsTestPageInitial");
            for(int h=0; h < skillsTestInitialCheck.length ; h++)
            {
                cukesWorld.getSoftAssert().assertTrue(skillsTestInitialCheck[h], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillsTestInitValue.split(",")[h]));
            }
        }

        boolean[] skillsQnGeneralCheck = skillsTest.createSkillsQuestionPageCheck();
        String skillQnValue = skillsTest.getSkillsTestJsonValue("SkillsQnPageGeneral");
        for(int j=0; j < skillsQnGeneralCheck.length ; j++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsQnGeneralCheck[j], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillQnValue.split(",")[j]));
        }

        boolean[] skillsTestCreationCheck = skillsTest.createSkillsQuestionsCheck(System.getProperty("test.image.url"));
        String skillQnCreationValue = skillsTest.getSkillsTestJsonValue("SkillsQnCreationPage");
        for(int k=0; k < skillsTestCreationCheck.length ; k++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsTestCreationCheck[k], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillQnCreationValue.split(",")[k]));
        }

        boolean[] skillsTestGeneralCheck = skillsTest.skillsTestPageCheck();
        String skillsTestValue = skillsTest.getSkillsTestJsonValue("SkillsTestPageGeneral");
        for(int i=0; i < skillsTestGeneralCheck.length ; i++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsTestGeneralCheck[i], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillsTestValue.split(",")[i]));
        }

        boolean[] skillsTestSearchCheck = skillsTest.skillsTestPageSearchCheck(getDataFromJson("runData","SkillQuestion","qnTitle"),getDataFromJson("runData","SkillQuestion","noOfAnswers"),getDataFromJson("runData","SkillQuestion","answerTimeMin"),getDataFromJson("runData","SkillQuestion","answerTimeSec"));
        String skillsTestSearchValue = skillsTest.getSkillsTestJsonValue("SkillsTestPageSearch");
        for(int l=0; l < skillsTestSearchCheck.length ; l++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsTestSearchCheck[l], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillsTestSearchValue.split(",")[l]));
        }

        boolean[] skillsTestOptionsCheck = skillsTest.skillsTestPageOptionsCheck(getDataFromJson("runData","SkillQuestion","qnTitle"),getDataFromJson("runData","SkillQuestion","noOfAnswers"),getDataFromJson("runData","SkillQuestion","appendValue"));
        String skillsTestOptionValue = skillsTest.getSkillsTestJsonValue("SkillsTestPageOptions");
        for(int m=0; m < skillsTestOptionsCheck.length ; m++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsTestOptionsCheck[m], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillsTestOptionValue.split(",")[m]));
        }
    }

    @And("User creates the skill test")
    public void userCreatesTheSkillTest() throws IOException {
        boolean[] skillsTestSectionCheck = skillsTest.skillsTestsSectionOpen(getDataFromJson("runData","SkillQuestion","qnTitle"),getDataFromJson("runData","SkillQuestion","noOfAnswers"),getDataFromJson("runData","SkillQuestion","appendValue"));
        skillsTest = new skillsTestCreationPage(cukesWorld.getDriver());
        String skillsTestSectionValue = skillsTest.getSkillsTestJsonValue("SkillsTestSectionPage");
        for(int n=0; n < skillsTestSectionCheck.length ; n++)
        {
            cukesWorld.getSoftAssert().assertTrue(skillsTestSectionCheck[n], "Appearance verification of "+skillsTest.getSkillsTestJsonValue(skillsTestSectionValue.split(",")[n]));
        }

        skillsTest.skillsTestDelete(getDataFromJson("runData","SkillsTest", "TestName"),"Title_Test_2024-01-24T14:58:21.571070766+05:30[Asia/Kolkata]");
    }

    @And("User creates the project")
    public void userCreatesTheProject() {
    }

    @And("User adds the form into the project")
    public void userAddsTheFormIntoTheProject() {
    }

    @And("User activates the project")
    public void userActivatesTheProject() {
    }



}
