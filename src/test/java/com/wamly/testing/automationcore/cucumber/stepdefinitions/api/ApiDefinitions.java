package com.wamly.testing.automationcore.cucumber.stepdefinitions.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.api.API_E2E;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import java.io.IOException;

public class ApiDefinitions extends Base {

    JsonNode skillTestResponse;
    JsonNode formResponse;

    @When("User Login to the application with valid credentials")
    public void callLoginAPI() {
        API_E2E apiUtils = new API_E2E();
        apiUtils.getTokenID(getDataFromJson("apiData","login","loginApi"));
    }

    @Then("User should be logged in successfully")
    public void getStatusCode() {
        API_E2E apiUtils = new API_E2E();
        int statusCode = apiUtils.getStatusCode(Integer.parseInt(getDataFromJson("runData","API","statusToken")));
        Assert.assertEquals(statusCode, 200, "Unexpected status code");
    }


    @Given("User created a form through form api")
    public void callFormApi() {
        API_E2E apiUtils = new API_E2E();
        apiUtils.createForm(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","form","formApi"));
        formResponse = apiUtils.getForm(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","form","getForm")+getDataFromJson("runData","API","formid"));
    }

    @When("User calls the skill test questions api")
    public void callSkillTestQuestionsApi() {
        API_E2E apiUtils = new API_E2E();
        apiUtils.createSkillTestQuestions(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","skillTest","skillTestQuestions"));
    }

    @When("User calls the skill test api")
    public void callSkillTestApi() {
        API_E2E apiUtils = new API_E2E();
        skillTestResponse = apiUtils.createSkillTest(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","skillTest","skillTestApi"),getDataFromJson("runData","API", "skillTestQ1Id"),getDataFromJson("runData","API", "skillTestQ2Id"),getDataFromJson("runData","API", "skillTestQ3Id"));
    }

    @When("User calls the project creation api")
    public void callProjectCreationApi() {
        API_E2E apiUtils = new API_E2E();
        apiUtils.createProject(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","project","projectCreation"),skillTestResponse);
    }

    @When("User calls the api for adding a form into the project")
    public void callProjectUpdateApi() throws IOException {
        API_E2E apiUtils = new API_E2E();
        apiUtils.updateProject(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","project","projectUpdate"),formResponse,skillTestResponse,getDataFromJson("runData","API","ProjectName"),getDataFromJson("runData","API","ProjectCreationId"));
    }

    @When("User calls project activation api")
    public void callProjectActivationApi() {
        API_E2E apiUtils = new API_E2E();
        apiUtils.activateProject(getDataFromJson("runData","API","idToken"),getDataFromJson("apiData","base","baseApi"),getDataFromJson("apiData","project","projectActivation")+getDataFromJson("runData","API","ProjectCreationId")+"/activate");
    }
}
