package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {

    private final CukesWorld cukesWorld;

    public Hooks() {
        this.cukesWorld= RunnerCucumberTest.cukesWorld.get();
    }

    @After
    public void takeScreenshot(Scenario scenario){
        if (scenario.isFailed() && null != cukesWorld.getDriver().getDriver()) {
            byte[] screenshot = ((TakesScreenshot) cukesWorld.getDriver().getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
        }
    }


}
