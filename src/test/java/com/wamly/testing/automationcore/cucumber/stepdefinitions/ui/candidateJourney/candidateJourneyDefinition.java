package com.wamly.testing.automationcore.cucumber.stepdefinitions.ui.candidateJourney;

import com.wamly.testing.automationcore.cucumber.CukesWorld;
import com.wamly.testing.automationcore.cucumber.runners.RunnerCucumberTest;
import com.wamly.testing.automationcore.cucumber.stepdefinitions.Base;
import com.wamly.testing.automationcore.pages.api.API_E2E;
import com.wamly.testing.automationcore.pages.ui.candidateJourney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class candidateJourneyDefinition extends Base {

    private CukesWorld cukesWorld;
    interviewLandingPage navigate;
    skillTestPage skillTest;
    preInterviewCheck precheck;
    interviewPage interview;
    String uiSkillsTestQuestionList;

    public candidateJourneyDefinition() {
        this.cukesWorld= RunnerCucumberTest.cukesWorld.get();
    }

    @And("Candidate launches the interview link")
    public void candidateLaunchesTheInterviewLink(){
        cukesWorld.getDriver().navigate(getDataFromJson("runData","API","ProjectGenericLink"));
    }

    @Then("Candidate should see the explainer video")
    public void candidateShouldSeeTheExplainerVideo() {
        navigate = new interviewLandingPage(cukesWorld.getDriver());
        navigate.explainerVideoOpen();

        //Check the visibility of the video
        cukesWorld.getSoftAssert().assertTrue(navigate.videoVisibilityCheck("VideoPlayButton"),"Appearance verification of Explainer Video");
        cukesWorld.getSoftAssert().assertAll();

        navigate.explainerVideoPlayClose();
    }

    @And("Candidate navigates through the welcome page with valid credentials")
    public void candidateNavigatesThroughTheWelcomePageWithValidCredentials() {
        //Required credentials provided
        navigate.navigateInterviewLandingPage();

        //Assertions for all values on the Welcome Page
        String[] uiElementsInterviewLandingPage = { "EmailAddress", "ConsentCheckbox", "ProceedMessage", "NextButton" };
        String[] msgElementInterviewLandingPage = { "Email Address", "Consent Checkbox", "'Please accept the consent form and enter your email address to proceed:' message", "'Next' Button" };
        int landMsgPos = 0;
        for(String uiLandingPageElements : uiElementsInterviewLandingPage)
        {
            cukesWorld.getSoftAssert().assertTrue(navigate.VisibilityCheck(uiLandingPageElements),"Appearance verification of " + msgElementInterviewLandingPage[landMsgPos]);
            landMsgPos++;
        }
        cukesWorld.getSoftAssert().assertAll();
    }

    @Then("Candidate Validates the Project name on the Dashboard")
    public void candidateValidatesTheProjectNameOnTheDashboard() throws IOException {
        cukesWorld.getSoftAssert().assertTrue(navigate.ValidationCheck("ProjectName").equals(getSimpleJsonValue("createProject", "projectName")), "Appearance verification of '" + getSimpleJsonValue("createProject", "projectName") + "' as the Project name");
        cukesWorld.getSoftAssert().assertAll();
    }

    @And("Candidate fetches the OTP from email")
    public void candidateFetchesTheOTPFromEmail() {

        navigate.nextPage();
        ZonedDateTime otpMailsentTime = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Z"));
        API_E2E otp=new API_E2E();

        //Fetching the OTP from candidate email id
        otp.otpFetch(otpMailsentTime, System.getProperty("test.mailsac.baseUrl"), System.getProperty("test.mailsac.endUrl"));
        //otp.deleteAllEmail(System.getProperty("test.mailSac.baseUrl"), System.getProperty("test.mailSac.endUrl"));
    }

    @And("Candidate enters a valid OTP and clicks Next")
    public void candidateEntersAValidOTPAndClicksNext() {

        //Input the correct OTP
        navigate.otpPaste(getDataFromJson("runData","OTP","commenceOTP"));

        //Assertions for all elements on the OTP Page
        cukesWorld.getSoftAssert().assertTrue(navigate.ValidationCheck("EmailId").equals(System.getProperty("test.candidate.username")),"Appearance verification of '"+System.getProperty("test.candidate.username")+"' Email Id");
        String[] uiElementsEnterOTPPage = { "MakingSureTitle", "OtpSentMessage", "EnterOTP", "NextButton" };
        String[] msgElementEnterOTPPage = { "'Let’s make sure it's you' title", "'A one-time pin (OTP) has been sent to' message", "'Enter OTP' field", "'Next' Button" };
        int otpmsgpos = 0;
        for(String uiLandingPageElements : uiElementsEnterOTPPage)
        {
            cukesWorld.getSoftAssert().assertTrue(navigate.VisibilityCheck(uiLandingPageElements),"Appearance verification of " + msgElementEnterOTPPage[otpmsgpos]);
            otpmsgpos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        navigate.nextPage();
    }

    @And("Candidate enters an invalid OTP and clicks Next")
    public void candidateEntersAnInvalidOTPAndClicksNext() {
        navigate.nextPage();

        //Input the incorrect OTP
        navigate.wrongOtpPaste();

    }

    @Then("Candidate views the Invalid OTP Error message")
    public void candidateEntersViewsTheInvalidOTPErrorMessage() {
        //Assertion for observing Wrong OTP Error message.
        cukesWorld.getSoftAssert().assertTrue(navigate.VisibilityCheck("WrongOtpErrorMessage"),"Appearance verification of Wrong OTP Error Message");
    }

    @Then("Candidate should see the Intro video and proceed")
    public void candidateShouldSeeTheWelcomeVideoAndProceed() {
        //Assertions for the visibility of Introduction video
        cukesWorld.getSoftAssert().assertTrue(navigate.videoVisibilityCheck("IntroVideo"),"Appearance verification of Introduction Video");
        cukesWorld.getSoftAssert().assertAll();
        navigate.introVideo();
    }

    @And("Candidate navigates through the personal information page")
    public void candidateNavigatesThroughThePersonalInformationPage() throws IOException {
        personalInfoPage info = new personalInfoPage(cukesWorld.getDriver());

        //All Personal information filled into the form
        info.personalInfoFormFill();

        //Assertions for all elements on the Personal Information Page
        cukesWorld.getSoftAssert().assertTrue(info.VisibilityCheck("FormHeading"),"Appearance verification of 'Update your details to proceed' form heading");
        String[] formValues = jsonRead("formapi","fields","name");
        String uiFormFields = info.uiFieldsFetch();
        String singleFormValue;
        for (String formValue : formValues) {
            singleFormValue = formValue.trim();
            cukesWorld.getSoftAssert().assertTrue(uiFormFields.contains(singleFormValue), "Appearance verification of " + singleFormValue);
        }
        cukesWorld.getSoftAssert().assertTrue(info.VisibilityCheck("NextButton"),"Appearance verification of 'Next' button");
        cukesWorld.getSoftAssert().assertAll();

        //Asserting that the candidate received 'Successful registration' email after the Personal Information Page
        String personalInfoCompleteTime = String.valueOf(info.nextPage());
        API_E2E emailCheck = new API_E2E();
        String subject = emailCheck.candidateEmailStatusFetch("Successful registration", personalInfoCompleteTime, System.getProperty("test.mailsac.baseUrl"), System.getProperty("test.mailsac.endUrl"));
        cukesWorld.getSoftAssert().assertTrue(subject.equals("Successful registration"),"Email verification of 'Successful registration' message");
        cukesWorld.getSoftAssert().assertAll();
    }

    @Then("Candidate should see the Skills Test Info Page")
    public void candidateShouldSeeTheSkillsTestInfoPage() {
        skillTest = new skillTestPage(cukesWorld.getDriver());

        //Assertions for all elements on the Skills Test Info Page
        String[] uiElementsCommenceSkillTestPage = { "SkillsTestHeading", "HowItWorks", "NoOfQuestions", "SkillQnTotalTime", "CommenceSkillTest"};
        String[] msgElementsCommenceSkillTestPage = { "'Skills Test' Heading", "'How It Works' description", "'Number of questions' column", "'Total time for the skills test' column", "'Start the test' Button"};
        int sklTstMsgPos = 0;
        for(String uiCommenceSkillTestPageElements : uiElementsCommenceSkillTestPage)
        {
            cukesWorld.getSoftAssert().assertTrue(skillTest.VisibilityCheck(uiCommenceSkillTestPageElements),"Appearance verification of " + msgElementsCommenceSkillTestPage[sklTstMsgPos]);
            sklTstMsgPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Commencing the Skills Test
        skillTest.commenceSkillTest();
    }

    @And("Candidate attends the skill test successfully")
    public void candidateAttendsTheSkillTestSuccessfully() throws IOException {
        //Attending the Skills Test with Correct Answers
        uiSkillsTestQuestionList = skillTest.attendSkillTest(skillTestFetch("Questions"), skillTestFetch("Correct Answers"));
    }

    @And("Candidate attends the skill test and fails")
    public void candidateAttendsTheSkillTestUnsuccessfully() throws IOException {
        //Attending the Skills Test with Wrong Answers
        uiSkillsTestQuestionList = skillTest.failSkillTest(skillTestFetch("Questions"), skillTestFetch("Correct Answers"));
    }

    @Then("Candidate validates all skill test questions")
    public void candidateValidatesAllSkillTestQuestions() throws IOException {
        //Fetching all Skills Test Questions
        String[] questionList = skillTestFetch("Questions");

        //Assertions for all Skills Test Questions
        for (String questionElement : questionList) {
            cukesWorld.getSoftAssert().assertTrue(uiSkillsTestQuestionList.contains(questionElement), "Appearance verification of '" + questionElement + "' Skills Test Question");
        }
        cukesWorld.getSoftAssert().assertAll();
    }

    @And("Candidate navigates back to the Dashboard")
    public void candidateNavigatesBackToTheDashboard() {
        //Assertions for all elements on Back to Dashboard Page after the skills test failure
        String[] uiElementsFailedSkillTestPage = { "SkillsTestFailHeading", "SkillsTestFailDescription", "BackToDashboardButton"};
        String[] msgElementsFailedSkillTestPage = { "'Great job! You have finished your skills test.' message", "'Thank you for participating' message", "'Back to dashboard' Button"};
        int sklTstFailMsgPos = 0;
        for(String uiFailSkillTestPageElements : uiElementsFailedSkillTestPage)
        {
            cukesWorld.getSoftAssert().assertTrue(skillTest.VisibilityCheck(uiFailSkillTestPageElements),"Appearance verification of " + msgElementsFailedSkillTestPage[sklTstFailMsgPos]);
            sklTstFailMsgPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Dashboard
        skillTest.backToDashboard();
    }

    @And("Candidate proceeds to the pre interview checks")
    public void candidateProceedsToThePreInterviewChecks() {
        precheck = new preInterviewCheck(cukesWorld.getDriver());

        //Proceeding to the Pre Interview Tests Page
        precheck.proceedToPreInterviewTests();

        //Assertions for all elements on the Pre Interview Tests Page
        String[] uiPreInterviewPage = { "TestDescription", "ConnectionTest", "HardwareTest", "TestQuestion", "ProceedToPreInterviewButton"};
        String[] msgPreInterviewPage = { "Test Description", "'Connection Test' column", "'Hardware Test' column", "'TestQuestion' column", "'Proceed To Pre-Interview' Button"};
        int preIntrvwMsgPos = 0;
        for(String uiPreInterviewElements : uiPreInterviewPage)
        {
            cukesWorld.getSoftAssert().assertTrue(precheck.VisibilityCheck(uiPreInterviewElements),"Appearance verification of " + msgPreInterviewPage[preIntrvwMsgPos]);
            preIntrvwMsgPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Pre Interview Tests
        precheck.preInterviewCheckProceed();
    }

    @And("Candidate navigates through the browser check")
    public void candidateNavigatesThroughTheBrowserCheck() {
        //Assertions for all elements on the Connection and Browser Check Page
        String[] uiBrowserCheckPage = { "ConnectionCheck", "ConnectionTag", "DownloadTag", "UploadTag", "BrowserTag", "ProceedToHardwareTestButton"};
        String[] msgBrowserCheckPage = { "Connection check heading", "'Connection' tag after Upload and Download test", "'Download' tag after Download test", "'Upload' tag after Upload test", "'Browser' tag after Browser version test", "'Next step: test your hardware' Button"};
        int brwsrCheckMsgPos = 0;
        for(String uiBrowserCheckPageElements : uiBrowserCheckPage)
        {
            cukesWorld.getSoftAssert().assertTrue(precheck.VisibilityCheck(uiBrowserCheckPageElements),"Appearance verification of " + msgBrowserCheckPage[brwsrCheckMsgPos]);
            brwsrCheckMsgPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Hardware Tests
        precheck.proceedToHardwareTest();
    }

    @And("Candidate navigates through the camera and microphone check")
    public void candidateNavigatesThroughTheCameraAndMicrophoneCheck() {
        //Assertions for all elements on the Hardware Check Page before Camera and Microphone Testing
        String[] uiCameraMicPage = { "CheckingHardwareHeading", "RecordingTest", "MicrophoneOption", "CameraOption", "MicrophoneDropdown", "CameraDropdown", "DisabledButton"};
        String[] msgCameraMicPage = { "Checking hardware heading", "Recording test Button", "Microphone Option", "Camera Option", "Microphone Dropdown", "Camera Dropdown", "DisabledButton"};
        int cameraMicPos = 0;
        for(String uiCameraMicElements : uiCameraMicPage)
        {
            cukesWorld.getSoftAssert().assertTrue(precheck.VisibilityCheck(uiCameraMicElements),"Appearance verification of " + msgCameraMicPage[cameraMicPos]);
            cameraMicPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Performing Camera and Microphone Tests
        precheck.cameraMicrophoneTest();

        //Assertions for all elements on the Hardware Check Page after Camera and Microphone Testing
        String[] uiCameraMicAfterCheckPage = { "ReTestRecordingButton", "MicrophoneGood", "CameraGood", "ConsentCheckbox", "NextButton"};
        String[] msgCameraMicAfterCheckPage = { "'Re-test recording' Button", "'Microphone seems good to go!' message", "'Camera seems good to go!' message", "Consent Checkbox", "'Next' Button"};
        int cameraMicAfterCheckPos = 0;
        for(String uiCameraMicAfterCheckElements : uiCameraMicAfterCheckPage)
        {
            cukesWorld.getSoftAssert().assertTrue(precheck.VisibilityCheck(uiCameraMicAfterCheckElements),"Appearance verification of " + msgCameraMicAfterCheckPage[cameraMicAfterCheckPos] + " after Camera and Microphone Test");
            cameraMicAfterCheckPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        precheck.nextPage();
    }

    @And("Candidate goes into the practice test and interface tour")
    public void candidateGoesIntoThePracticeTestAndInterfaceTour() {
        interview = new interviewPage(cukesWorld.getDriver());

        //Assertions for all elements on the Successful Hardware Check Page
        String[] uiPreInterviewPage = { "HardwareGoodConfirmation", "HardwareConnectionGoodDescription", "NextStepInstruction", "PracticeInterview", "CommenceInterview"};
        String[] msgPreInterviewPage = { "'Your hardware is good' confirmation message", "Hardware and Connection are good description", "Instruction for Next Steps", "'Practice' Button", "'Start interview' Button"};
        int preInterviewPos = 0;
        for(String uiPreInterviewElements : uiPreInterviewPage)
        {
            cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck(uiPreInterviewElements),"Appearance verification of " + msgPreInterviewPage[preInterviewPos] + " after Camera and Microphone Test");
            preInterviewPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Pre Interview Page
        interview.preInterviewPage();

        //Assertions for all elements on the Pre Interview Page
        cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck("CommencePracticeInterview"),"Appearance verification of 'Start' Button");
        cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck("InterfaceTourTopic"),"Appearance verification of 'Interface Tour' heading");

        //Commencing the Practice Interview
        interview.commencePracticeInterview();

        //Assertions for all elements through the Interface Tour
        String[] uiInterfaceTourPage = { "Preparation Screen & Time", "Question", "Skip Preparation Time", "Countdown Timer", "Blur your screen", "Next Question"};
        for (String interfaceElement : uiInterfaceTourPage) {
            cukesWorld.getSoftAssert().assertTrue(interview.interfaceTour().contains(interfaceElement), "Appearance verification of '" + interfaceElement + "' heading");
        }
        cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck("AllDoneMessage"),"Appearance verification of 'You’re all done!' heading");
        cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck("PracticeInterviewSampleQuestionTry"),"Appearance verification of 'Try a test question' Button");
        cukesWorld.getSoftAssert().assertAll();

        //Assertions for all elements through the Practice Interview
        String uiPracticeInterviewPageFetch = interview.practiceInterview();
        String[] uiPracticeInterviewPage = { "ANSWER TIME (Sec)", "Start recording now", "Blur Screen", "Next Question"};
        for (String PracticeElement : uiPracticeInterviewPage) {
            cukesWorld.getSoftAssert().assertTrue(uiPracticeInterviewPageFetch.contains(PracticeElement), "Appearance verification of " + PracticeElement);
        }
        cukesWorld.getSoftAssert().assertAll();
    }

    @Then("Candidate starts the interview")
    public void candidateLoginToStartTheInterview() {

        //Assertions for all elements on the Practice Interview Completion Page
        String[] uiPracticeCompletePage = { "TestCompleteConfirmation", "TestCompleteDescription", "SecondNextStepInstruction", "CommenceInterview", "PracticeAgainButton"};
        String[] msgPracticeCompletePage = { "Test Complete Confirmation", "Hardware and Connection are good description", "Instruction for Next Steps", "'Start interview' Button", "'Practice again' Button" };
        int practiceCompletePos = 0;
        for(String uiPracticeCompletePageElements : uiPracticeCompletePage)
        {
            cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck(uiPracticeCompletePageElements),"Appearance verification of " + msgPracticeCompletePage[practiceCompletePos] + " after Camera and Microphone Test");
            practiceCompletePos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Interview Page
        interview.commenceInterview();
    }

    @And("Candidate attends the interview")
    public void candidateAttendsTheInterview() throws IOException {

        //Attending the Interview and Assertions for all Interview Questions
        String[] formInterviewQuestions = jsonRead("createProject","questions","question");
        String uiInterviewQuestions = interview.attendInterview(formInterviewQuestions);
        for (String formInterviewQuestionElement : formInterviewQuestions) {
            cukesWorld.getSoftAssert().assertTrue(uiInterviewQuestions.contains(formInterviewQuestionElement), "Appearance verification of '" + formInterviewQuestionElement + "' Interview Question");
        }
    }

    @Then("Candidate receives the Interview completed email")
    public void candidateReceivesTheInterviewCompletedEmail() {

        //Asserting that the candidate received 'Done and dusted' email after the Interview is completed
        API_E2E completeEmailCheck = new API_E2E();
        String subject = completeEmailCheck.candidateEmailStatusFetch("Done and dusted", getDataFromJson("runData","TimeStamp","DoneStamp"), System.getProperty("test.mailsac.baseUrl"), System.getProperty("test.mailsac.endUrl"));
        cukesWorld.getSoftAssert().assertTrue(subject.equals("Done and dusted"),"Email verification of 'Done and dusted' message");
        cukesWorld.getSoftAssert().assertAll();
    }

    @And("Candidate rates the experience")
    public void candidateRatesTheExperience() {

        //Assertions for all elements on the Experience Feedback Page
        String[] uiReviewPage = { "ThankYouMessage", "ReviewRequest", "LovedItReview", "CouldBeBetterReview", "ReviewPrivacyAssurance"};
        String[] msgReviewPage = { "Thank you message", "Review Request", "'Loved it!' review option", "'Could be better' review option", "Privacy assurance of Review message"};
        int reviewPagePos = 0;
        for(String uiReviewPageElements : uiReviewPage)
        {
            cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck(uiReviewPageElements),"Appearance verification of " + msgReviewPage[reviewPagePos]);
            reviewPagePos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Providing a Random Feedback
        interview.interviewFeedback();
    }

    @And("Candidate goes back to the dashboard login page")
    public void candidateGoesBackToTheDashboardLoginPage() {
        //Assertions for all elements on Back to Dashboard Page after the feedback is submitted
        String[] uiDashboardNavPage = { "FeedbackThankYouMessage", "DashboardLink", "BackToDashboardMessage"};
        String[] msgDashboardNavPage= { "'Thanks for the feedback' message", "'Click here' dashboard link", "'to go back to your dashboard.' message"};
        int dashboardNavPos = 0;
        for(String uiDashboardNavElements : uiDashboardNavPage)
        {
            cukesWorld.getSoftAssert().assertTrue(interview.VisibilityCheck(uiDashboardNavElements),"Appearance verification of " + msgDashboardNavPage[dashboardNavPos]);
            dashboardNavPos++;
        }
        cukesWorld.getSoftAssert().assertAll();

        //Proceeding to the Dashboard
        interview.dashboardLink();
    }

    @And("Candidate Login to the dashboard")
    public void candidateLoginToTheDashboard() {
        //Assertions for all elements on the Dashboard Welcome Page
        cukesWorld.getSoftAssert().assertTrue(navigate.ValidationCheck("DashboardWelcome").contains(System.getProperty("test.candidate.username")), "Appearance verification of '" + System.getProperty("test.candidate.username") + "' Email Id");
        cukesWorld.getSoftAssert().assertTrue(navigate.VisibilityCheck("DashboardWelcome"), "Appearance verification of 'Welcome' message");
        cukesWorld.getSoftAssert().assertTrue(navigate.VisibilityCheck("OrganisationSelect"), "Appearance verification of 'Select an organisation below to view more details:' prompt");
        cukesWorld.getSoftAssert().assertAll();
    }

    @And("Candidate Views the Project Name on the Dashboard")
    public void candidateViewsTheProjectNameOnTheDashboard() throws IOException {
        //Assertions for Candidate Interview details on the Dashboard Page
        String projectName = getSimpleJsonValue("createProject", "projectName");
        skillTest.findProjectDropdown(projectName);
        cukesWorld.getSoftAssert().assertTrue(projectName.equals(skillTest.projectNameVerify(projectName)), "Appearance verification of '" + projectName + "' as the Project name");
        cukesWorld.getSoftAssert().assertAll();
    }

    @Then("Candidate Views the interview status as Complete")
    public void candidateViewsTheInterviewStatusAsComplete() throws IOException {
        //Assertions for Candidate Interview status on the Dashboard Page
        cukesWorld.getSoftAssert().assertTrue(skillTest.projectStatusVerify(getSimpleJsonValue("createProject", "projectName")).equals("COMPLETED"),"Appearance verification of Interview 'COMPLETED' Status");
        cukesWorld.getSoftAssert().assertAll();
    }

}
