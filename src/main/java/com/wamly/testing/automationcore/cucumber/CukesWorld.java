package com.wamly.testing.automationcore.cucumber;


import com.wamly.testing.automationcore.reports.SoftAssertExt;
import com.wamly.testing.automationcore.selenium.ParentDriver;

public class CukesWorld {
    private QTAFTestNGCucumberTest runner;

    public QTAFTestNGCucumberTest getRunner() {
        return runner;
    }

    public void setRunner(QTAFTestNGCucumberTest runner) {
        this.runner = runner;
    }

    public ParentDriver getDriver(){
        return this.getRunner().getDriver();
    }

    public SoftAssertExt getSoftAssert(){
        return this.getRunner().getSoftAssert();
    }
}
