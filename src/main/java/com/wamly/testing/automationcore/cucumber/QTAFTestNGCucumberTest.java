package com.wamly.testing.automationcore.cucumber;

import com.wamly.testing.automationcore.testng.BaseTest;
import io.cucumber.testng.*;
import org.apiguardian.api.API;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.xml.XmlTest;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;

@API(
        status = API.Status.STABLE
)
public abstract  class QTAFTestNGCucumberTest extends BaseTest {
    private TestNGCucumberRunner testNGCucumberRunner;
    public static final ThreadLocal<CukesWorld> cukesWorld = new ThreadLocal<>();

    protected QTAFTestNGCucumberTest() {
    }


    @BeforeClass(alwaysRun = true)
    public void setUpClass() {
        cukesWorld.set(new CukesWorld());
        cukesWorld.get().setRunner(this);
    }

    @BeforeClass(
            alwaysRun = true
    )
    public void setUpClass(ITestContext context) {
        XmlTest currentXmlTest = context.getCurrentXmlTest();
        Objects.requireNonNull(currentXmlTest);
        CucumberPropertiesProvider properties = currentXmlTest::getParameter;
        this.testNGCucumberRunner = new TestNGCucumberRunner(this.getClass(), properties);
    }

    @Test(
            groups = {"cucumber"},
            description = "Runs Cucumber Scenarios",
            dataProvider = "scenarios"
    )
    public void runScenario(PickleWrapper pickleWrapper, FeatureWrapper featureWrapper) {
        this.testNGCucumberRunner.runScenario(pickleWrapper.getPickle());
    }

    @DataProvider
    public Object[][] scenarios() {
        return this.testNGCucumberRunner == null ? new Object[0][0] : this.testNGCucumberRunner.provideScenarios();
    }

    @AfterClass(
            alwaysRun = true
    )
    public void tearDownClass() {
        if (this.testNGCucumberRunner != null) {
            this.testNGCucumberRunner.finish();
        }
    }

    @AfterSuite
    public void clearJson(){
        String fileName = "src"+ File.separator+"test"+File.separator+"resources"+File.separator+"com"+File.separator+"wamly"+File.separator+"testing"+File.separator+"automationcore"+File.separator+"testdata"+File.separator+"runTimeData"+File.separator+"testdata.json";
        // Read the JSON content from the file
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(fileName));

            // Clear all values by looping through keys
            Iterator<?> keys = jsonObject.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                JSONObject emptyObject = new JSONObject();
                jsonObject.put(key, emptyObject);
            }

            // Write the updated JSON back to the file
            try (FileWriter file = new FileWriter(fileName)) {
                file.write(jsonObject.toString()); // Corrected line
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
