package com.wamly.testing.automationcore.selenium;

import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.reports.ExtentReport;
import com.wamly.testing.automationcore.utils.TestLog;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Set;

public abstract class ParentDriver {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public String getTitle() {
        return driver.getTitle();
    }

    public enum ScreenshotFileType {BASE64, BYTES, FILE}
    public enum Permission{ALLOW {@Override public String toString(){return "Allow";}},BLOCK{@Override public String toString(){return "Block";}}}


    public abstract void open();

    public abstract WebDriver getDriver();

    public abstract Element findElement(By by);

    public abstract List<Element> findElements(By by);

    public abstract Element findVisibleElement(By by);

    public abstract Element findClickableElement(By by);

    public abstract Element findPresentElement(By by);

    public abstract void givePermission(Permission permission);

    public abstract boolean saveScreenRecording(String media, String outputDir, String outputFileName);

    public abstract void takeScreenshot(String imagePath);

    public abstract Object getScreenshotAs(ScreenshotFileType opTyp);

    public abstract void startScreenRecording();

    public abstract Object stopScreenRecording();

    public final void navigate(String url) {
        TestLog.log().info("Navigate to application url : {}", url);
        if (driver != null) driver.get(url);
        else {
            throw new FrameworkException("Browser Driver is not initialised and opened. Browser should be opened before calling navigate()");
        }
        ExtentReport.log(String.format("Navigate to the application url: %s", url));
    }

    public void close() {
        if (driver != null) driver.quit();
    }

    public void staticWait(long duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            TestLog.log().warn("Interrupted!", e);
            Thread.currentThread().interrupt();
        }
    }

    public void click(Element element) {
        try {
            element.click();
        } catch (ElementClickInterceptedException e) {
            element.jsClick();
        }
    }

    /** Get the Selenium Browser window handle ...*/
    public abstract String getWindowHandle();

    /** Get the Selenium Browser window handle ...*/
    public abstract Set<String>  getWindowHandles();

    /** Instructs the WebDriver to switch to parent window ...*/
    public abstract void switchToParentWindow(String parentWindowHandle);

    /** Instructs the browser to switch to a window using title..*/
    public abstract  boolean switchToNewWindowUsingTitle(String windowTitle);

    /** Instructs the browser to switch to the last window..*/
    public abstract  void switchToLastWindow();

    /** Set WebDriver wait timeout with required time in milliseconds**/
    public void setWebDriverWaitTime(long milliseconds){

        wait = new WebDriverWait(driver, Duration.ofMillis(milliseconds));
    }
}
