package com.wamly.testing.automationcore.selenium.browser;

import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ElementImpl;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.utils.TestLog;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class BrowserDriver extends ParentDriver {

    @Override
    public WebDriver getDriver() {
        return this.driver;
    }

    @Override
    public Element findElement(By by) {
        return new ElementImpl(driver.findElement(by));
    }

    @Override
    public List<Element> findElements(By by) {
        List<Element> elements = new ArrayList<>();
        List<WebElement> webElements = driver.findElements(by);
        for (WebElement webElement : webElements) {
            elements.add(new ElementImpl(webElement));
        }
        return elements;
    }

    @Override
    public Element findVisibleElement(By by) {
        return new ElementImpl(wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
    }

    @Override
    public Element findClickableElement(By by) {
        return new ElementImpl(wait.until(ExpectedConditions.elementToBeClickable(by)));
    }

    @Override
    public Element findPresentElement(By locator) {
        return new ElementImpl(wait.until((ExpectedCondition<WebElement>) driver -> {
            try {
                WebElement element = driver.findElement(locator);
                return element != null && element.isEnabled() ? element : null;
            } catch (StaleElementReferenceException | NotFoundException var4) {
                return null;
            }
        }));
    }

    @Override
    public void givePermission(Permission permission) {
        //Implementation pending
        throw new FrameworkException("This action is currently unsupported in the browser");
    }

    @Override
    public void takeScreenshot(String imagePath) {

    }

    @Override
    public Object getScreenshotAs(ScreenshotFileType opType) {
        switch (opType) {
            case BASE64:
                return ((TakesScreenshot) this.getDriver()).getScreenshotAs(OutputType.BASE64);
            case BYTES:
                return ((TakesScreenshot) this.getDriver()).getScreenshotAs(OutputType.BYTES);
            case FILE:
                return ((TakesScreenshot) this.getDriver()).getScreenshotAs(OutputType.FILE);
            default:
                throw new IllegalStateException("Unexpected value: " + opType);
        }
    }

    @Override
    public void startScreenRecording() {
        throw new FrameworkException("This action is currently unsupported in the browser");
    }

    @Override
    public Object stopScreenRecording() {
        throw new FrameworkException("This action is currently unsupported in the browser");
    }

    @Override
    public boolean saveScreenRecording(String media, String outputDir, String outputFileName) {
        throw new FrameworkException("This action is currently unsupported in the browser");
    }

    @Override
    /** Get the Selenium Browser window handle ...*/
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    /** Get the Selenium Browser window handle ...*/
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    /** Instructs the WebDriver to switch to parent window ...*/
    public void switchToParentWindow(String parentWindowHandle) {
        driver.switchTo().window(parentWindowHandle);
    }

    @Override
    /** Instructs the browser to switch to a window using title..*/
    public boolean switchToNewWindowUsingTitle(String windowTitle) {
        String parentWindowhHandle = getWindowHandle();
        boolean isMatchingWindowTitle = false;
        Set<String> handles = getWindowHandles();
        for (String windowHandle : handles) {
            if (!parentWindowhHandle.equalsIgnoreCase(windowHandle)) {
                driver.switchTo().window(windowHandle);
                try {
                    isMatchingWindowTitle = wait.until(ExpectedConditions.titleIs(windowTitle));
                    if (isMatchingWindowTitle)
                        TestLog.log().info("Switched to new window and matching window title found");
                    break;
                } catch (TimeoutException ignore) {
                    TestLog.log().info("Switched to new window but no matching window title found");
                }
            }
        }
        return isMatchingWindowTitle;
    }

    @Override
    /** Instructs the browser to switch to last window..*/
    public void switchToLastWindow() {

        ArrayList<String> windowHandles = new ArrayList<>(getWindowHandles());
        driver.switchTo().window(windowHandles.get(windowHandles.size() - 1));
    }
}

