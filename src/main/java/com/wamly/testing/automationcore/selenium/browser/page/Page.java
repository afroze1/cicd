package com.wamly.testing.automationcore.selenium.browser.page;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.restassured.ResponseUtils;
import com.wamly.testing.automationcore.selenium.Element;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.utils.JSON;
import com.wamly.testing.automationcore.utils.TestLog;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Page {
    protected ParentDriver driver;
    protected LocatorCollection locatorCollection;

    public Page(ParentDriver driver) {
        this.driver = driver;
    }

    public Element waitForElement(String locator, Integer time) {
        driver.setWebDriverWaitTime(time);
        Element Elmt = getElement(locator);
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return Elmt;
    }
    protected void setLocatorCollection() {
        String section = this.getClass().getName().replaceFirst("com.wamly.testing.automationcore.pages.ui.", "");
        String sectionName = section.substring(0, section.lastIndexOf("."));
        String locatorFile = System.getProperty("test.locator.path") + File.separator + sectionName + File.separator + this.getClass().getSimpleName().toLowerCase() + ".json";

        if (this.getClass().getResourceAsStream(locatorFile) == null) {
            TestLog.log().fatal("Failed to load locator file, please verify locator file against the class {}", this.getClass());
            throw new FrameworkException("Failed to load locator file, please verify locator file against the class");
        }
        locatorCollection = JSON.loadJSONAsObject(LocatorCollection.class, this.getClass().getResourceAsStream(locatorFile));
    }

    protected Element getElement(String element, String... formats) {
        Locator elementLocator = locatorCollection.getLocator(element);

        switch (elementLocator.locatorAttributes.locatorStrategy.toLowerCase()) {
            case "visible":
                return driver.findVisibleElement(getBy(elementLocator, formats));
            case "enabled":
                return driver.findPresentElement(getBy(elementLocator, formats));
            case "clickable":
                return driver.findClickableElement(getBy(elementLocator, formats));
            default:
                TestLog.log().warn("Locator Strategy {} not supported. Supported Strategies: visible, enabled, clickable. Element: {}", elementLocator.locatorAttributes.locatorStrategy, element);
                return driver.findElement(getBy(elementLocator));
        }

    }

    protected List<Element> getElements(String element, String... formats) {
        Locator elementLocator = locatorCollection.getLocator(element);
        return driver.findElements(getBy(elementLocator, formats));
    }

    private By getBy(Locator elementLocator, String... formats) {
        String locatorValue = String.format(elementLocator.locatorAttributes.locatorValue, formats);
        if (elementLocator.locatorAttributes.locatorType.equalsIgnoreCase("xpath"))
            return By.xpath(locatorValue);
        else if (elementLocator.locatorAttributes.locatorType.equalsIgnoreCase("css"))
            return By.cssSelector(locatorValue);
        else if (elementLocator.locatorAttributes.locatorType.equalsIgnoreCase("id"))
            return By.id(locatorValue);
        else if (elementLocator.locatorAttributes.locatorType.equalsIgnoreCase("name"))
            return By.name(locatorValue);
        else {
            TestLog.log().error("Unsupported Locator Type : {}. Supported Locator Types are : xpath, css, id, name", elementLocator.locatorAttributes.locatorType);
            throw new FrameworkException("Unsupported Locator Type");
        }
    }

    protected boolean isDisplayed(String locator) {
        driver.setWebDriverWaitTime(0);
        boolean isDisplayed = false;
        try {
            isDisplayed = getElement(locator).getSize().getHeight() > 0;
        } catch (TimeoutException ex) {
            isDisplayed = false;
        }
        driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return isDisplayed;
    }

    protected void waitForAPILoad(Long preTS, String path) {

        List<LogEntry> logs;
        List<LogEntry> currentLogEntries = new ArrayList<>();
        Map<String, Boolean> currentInterestedAPIsStatus = new HashMap<>();
        boolean interestedAPIsLoaded = false;

        boolean defaultTimeoutExceeded = (System.currentTimeMillis() - preTS) > Constants.DEFAULT_WAIT_TIMEOUT;

        while (!interestedAPIsLoaded && !defaultTimeoutExceeded) {

            defaultTimeoutExceeded = (System.currentTimeMillis() - preTS) > Constants.DEFAULT_WAIT_TIMEOUT;
            logs = (getLogs());
            currentLogEntries.clear();

            for (LogEntry logEntry : logs) {
                long entryTS = logEntry.getTimestamp();

                boolean typeXHR = logEntry.getMessage().contains("\"type\":\"XHR\"");
                boolean inTimeWindow = (preTS < entryTS);

                if (inTimeWindow && typeXHR) {
                    currentLogEntries.add(logEntry);
                }
            }

            if (currentInterestedAPIsStatus.isEmpty()) {
                for (LogEntry entry : currentLogEntries) {
                    String reqIdInterestedAPI = ResponseUtils.readValueFromJsonString(entry.getMessage(), path);
                    currentInterestedAPIsStatus.put(reqIdInterestedAPI, false);
                }
            }

            for (LogEntry entry : currentLogEntries) {
                for (Map.Entry<String, Boolean> intentry : currentInterestedAPIsStatus.entrySet()) {
                    boolean status200 = entry.getMessage().contains("\"status\":200");
                    if ((ResponseUtils.readValueFromJsonString(entry.getMessage(), path).equalsIgnoreCase(intentry.getKey())) && status200) {
                        intentry.setValue(true);
                    }
                }
            }

            for (Map.Entry<String, Boolean> entry : currentInterestedAPIsStatus.entrySet()) {
                if (Boolean.FALSE.equals(entry.getValue())) {
                    interestedAPIsLoaded = false;
                    driver.staticWait(500);
                    break;
                } else {
                    interestedAPIsLoaded = true;
                }
            }
        }

        if (interestedAPIsLoaded) {
            long postTS = System.currentTimeMillis();
            TestLog.log().info(String.format("APIs loaded with wait time : %d", postTS - preTS));
        } else {
            if (!currentLogEntries.isEmpty()) TestLog.log().info("APIs are not loaded in default timeout");
        }
    }

    protected List<LogEntry> getLogs() {
        return driver.getDriver().manage().logs().get(LogType.PERFORMANCE).getAll();
    }

    protected boolean isEnabled(String locator) {
        this.driver.setWebDriverWaitTime(0);
        boolean isEnabled = false;

        try {
            isEnabled = this.getElement(locator).isEnabled();
        } catch (TimeoutException ex) {
            isEnabled = false;
        }

        this.driver.setWebDriverWaitTime(Constants.DEFAULT_WAIT_TIMEOUT);
        return isEnabled;
    }

    public String getLoginJsonValue(String fieldName) throws IOException {
        return jsonDataRead("login"+File.separator+"loginpage", fieldName);
    }

    public String getFormsJsonValue(String fieldName) throws IOException {
        return jsonDataRead("projectCreation"+File.separator+"formcreationpage", fieldName);
    }

    public String getInterviewGuidesJsonValue(String fieldName) throws IOException {
        return jsonDataRead("interviewGuides"+File.separator+"interviewguidespage", fieldName);
    }

    public String getUserJsonValue(String fieldName) throws IOException {
        return jsonDataRead("user"+File.separator+"adduserpage", fieldName);
    }
    public String getSkillsTestJsonValue(String fieldName) throws IOException {
        return jsonDataRead("projectCreation"+File.separator+"skillstestcreationpage", fieldName);
    }

    public int generateRandomInteger(int lowerLimit, int upperLimit)
    {
        Random r = new Random();
        return (r.nextInt((upperLimit - lowerLimit) + 1) + lowerLimit);
    }

    public boolean visibilityCheck(Element ElementName)
    {
        return (ElementName.isEnabled());
    }


    public void switchFrameToIndex(int frameIndex) {
        driver.getDriver().switchTo().frame(frameIndex);
        new Actions(driver.getDriver()).moveToElement(driver.getDriver().switchTo().activeElement()).perform();
    }



    public void refreshPage() {
        driver.getDriver().get(driver.getDriver().getCurrentUrl());
    }

    public String jsonDataRead(String formName, String fieldName) throws IOException {

        String filePath1 = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "testdata" + File.separator;

        // Read the JSON content
        String jsonContent = readJsonFile(filePath1 + formName + ".json");

        // Parse the JSON content into a JSONObject
        org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);
        JSONArray jsonArray = jsonObject.getJSONArray("testdatas");

        String errorValue = null;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject testdata = (JSONObject) jsonArray.get(i);
            String errorName = (String) testdata.get("testdata.name");
            if (errorName.equals(fieldName)) {
                errorValue = (String) testdata.get("testdata.value");
                return errorValue;
            }
        }
        return null;
    }

    public void updateInJson(String module,String key, String updatedValue) {
        try {

            String filePath1 = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "testdata" + File.separator +"interviewGuides" + File.separator +  module + ".json" ;

            // Read the JSON content
            String jsonContent = readJsonFile(filePath1);


            // Parse the JSON content into a JSONObject
            JSONObject jsonObject = new JSONObject(jsonContent);

            // Update the value
            jsonObject.getJSONObject(module).put(key, updatedValue);

            // Write the updated JSON back to the file
            FileWriter fileWriter = new FileWriter(filePath1);
            fileWriter.write(jsonObject.toString());
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new FrameworkException(module+" - "+key +" is not updated in the json file");
        }
    }


    public String getValueFromJson(String module, String key) {
        try {
            String filePath1 = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "com" + File.separator + "wamly" + File.separator + "testing" + File.separator + "automationcore" + File.separator + "testdata" + File.separator +"interviewGuides" + File.separator +  module + ".json" ;

            // Read the JSON content
            String jsonContent = readJsonFile(filePath1);
            org.json.JSONObject jsonObject = new org.json.JSONObject(jsonContent);

            return jsonObject.getJSONObject(module).getString(key);
        } catch (IOException e) {
            e.printStackTrace();
            throw new FrameworkException("Error reading value from JSON file");
        }
    }



    public String readJsonFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }

}
