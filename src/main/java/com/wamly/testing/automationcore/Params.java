package com.wamly.testing.automationcore;

public class Params {

    protected Params(){}

    public static void setAppType(String argAppType) {
        if (argAppType!=null)
            System.setProperty(Constants.PROPERTY_NAME_APPTYPE, argAppType);
    }

    public static String getAppType() {
        return System.getProperty(Constants.PROPERTY_NAME_APPTYPE, Constants.DEFAULT_APP_TYPE);
    }

    public static boolean isWebOrDeviceTest() {
        return getAppType().equalsIgnoreCase("web")
                || getAppType().equalsIgnoreCase("mobile.native")
                || getAppType().equalsIgnoreCase("mobile.web");
    }

    public static boolean isDeviceTest() {
        return getAppType().equalsIgnoreCase("mobile.native")
                || getAppType().equalsIgnoreCase("mobile.web");
    }
}
