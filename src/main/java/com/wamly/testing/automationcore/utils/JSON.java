package com.wamly.testing.automationcore.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.wamly.testing.automationcore.FrameworkException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class JSON {

    private static final ObjectMapper mapper = new ObjectMapper();

    private JSON() {
    }

    public static <T> T loadJSONAsObject(Class<T> tClass, InputStream inputStream) {
        try {
            return mapper.readValue(inputStream, tClass);
        } catch (IOException e) {
            TestLog.log().fatal("Failed to load file against the class {}", tClass);
            throw new FrameworkException("Failed to load file against the class", e);
        }
    }

    public static <T> T loadJSONAsObject(Class<T> tClass, String json) {
        try {
            return mapper.readValue(json, tClass);
        } catch (JsonProcessingException e) {
            TestLog.log().fatal("Failed to convert JSON string to Java POJO class", e);
            throw new FrameworkException("Failed to convert JSON string to Java POJO class", e);
        }
    }

    /**
     * Read JSON data from a file and return it as a JsonNode
     *
     * @param jsonFile
     * @return
     * @throws IOException
     */
    public static JsonNode parseToJSONNode(File jsonFile) throws IOException {

        return mapper.readTree(jsonFile);
    }

    /**
     * Write a JsonNode to a JSON file
     *
     * @param jsonNode
     * @param filePath
     * @throws IOException
     */
    public static void writeJsonToFile(JsonNode jsonNode, String filePath) throws IOException {
        File file = new File(filePath);
        mapper.writeValue(file, jsonNode);
    }

    public static JsonNode convertToJsonArray(Map<String, JsonNode> nodeMap) {
        ArrayNode mergedArray = mapper.createArrayNode();
        nodeMap.values().forEach(mergedArray::add);
        return mergedArray;
    }

    public static JsonNode parseToJSONNode(String json) {
        JsonNode jsonNode = null;
        try {
            jsonNode = mapper.readTree(json);
        } catch (JsonProcessingException e) {
            TestLog.log().info("Unable to map the string to a valid JSON. Check if the given string is a valid JSON", e);
            throw new FrameworkException("Unable to map the string to a valid JSON. Check if the given string is a valid JSON", e);
        }
        return jsonNode;
    }

    public static String parseToString(JsonNode jsonNode) {

        String jsonAsString = null;
        try {
            jsonAsString = mapper.writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {
            TestLog.log().info("Unable to write Json Node as string. Check if the Json Node is valid", e);
            throw new FrameworkException("Unable to write Json Node as string. Check if the Json Node is valid", e);
        }
        return jsonAsString;
    }

    public static String updateJsonNode(JsonNode jsonNode, String pathToKey, String key, String value) {

        if (!StringUtil.isStartsWith(pathToKey, "/") && !pathToKey.isEmpty()) {

            pathToKey = StringUtil.append("/", pathToKey);
        }
        JsonNode childNode = jsonNode.at(pathToKey);
        ObjectNode objectNode = (ObjectNode) childNode;
        objectNode.put(key,  value);
        return jsonNode.toString();
    }

    public static String updateJsonNode(JsonNode jsonNode, String pathToKey, String key, JsonNode value) {

        if (!StringUtil.isStartsWith(pathToKey, "/") && !pathToKey.isEmpty()) {

            pathToKey = StringUtil.append("/", pathToKey);
        }
        JsonNode childNode = jsonNode.at(pathToKey);
        ObjectNode objectNode = (ObjectNode) childNode;
        objectNode.put(key, value);
        return jsonNode.toString();
    }

    public static String updateJsonNode(JsonNode jsonNode, String key, String value) {

        return updateJsonNode(jsonNode, "", key, value);
    }

    public static String updateJsonNode(JsonNode jsonNode, String key, JsonNode value) {

        return updateJsonNode(jsonNode, "", key, value);
    }

    public static String removeFromJsonNode(JsonNode jsonNode, String pathToKey, String key) {

        if (!StringUtil.isStartsWith(pathToKey, "/") && !pathToKey.isEmpty()) {

            pathToKey = StringUtil.append("/", pathToKey);
        }
        JsonNode childNode = jsonNode.at(pathToKey);
        ObjectNode objectNode = (ObjectNode) childNode;
        objectNode.remove(key);
        return jsonNode.toString();
    }

    public static String removeFromJsonNode(JsonNode jsonNode, String key) {

        return removeFromJsonNode(jsonNode, "", key);
    }
}
