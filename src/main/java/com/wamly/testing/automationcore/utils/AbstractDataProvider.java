package com.wamly.testing.automationcore.utils;

import java.util.Map;

public abstract class AbstractDataProvider {
    protected Map<String,String> dataMap;

    public abstract AbstractDataProvider setDataProvider(String fileName);
    public abstract String getData(String key);
}
