package com.wamly.testing.automationcore.utils;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class XrayIntegration {
    public static void xrayTestStatusUpdate() {
        // Your code to be executed after all scenarios in the suite

        String payload = convertCucumberReportToString();

        RestAssured.baseURI = System.getProperty("xray.baseApi");
        String authenticationEndPoint = System.getProperty("xray.xrayAuthenticationApi");
        String createExecutionEndPoint = System.getProperty("xray.createExecution");
        String graphQLEndpoint = System.getProperty("xray.graphQLApi");

        String authenticationPayload = System.getProperty("xray.autheticateBody");
        String graphQLPayload = System.getProperty("xray.graphqlBody");
        String clientID = System.getProperty("xray.client_id");
        String clientSecret = System.getProperty("xray.client_secret");
        //Decode the client ID and client Secret
        byte[] decodedClientID = Base64.getDecoder().decode(clientID);
        String decodedStringClientID = new String(decodedClientID);

        byte[] decodedSecret = Base64.getDecoder().decode(clientSecret);
        String decodedStringSecret = new String(decodedSecret);

        String authenticationAPIBody = String.format(authenticationPayload, decodedStringClientID, decodedStringSecret);
        //Call the Authentication API
        String apiToken =
                given().header("Content-Type", "application/json")
                        .body(authenticationAPIBody).when().post(authenticationEndPoint).then()
                        .assertThat().statusCode(200).extract().response().asString();

        apiToken = apiToken.replaceAll("^\"|\"$", "");
        //Execution of test cases
        String response =
                given().header("Content-Type", "application/json")
                        .header("accept", "application/json")
                        .header("Authorization", "Bearer " + apiToken).body(payload)
                        .when().post(createExecutionEndPoint).then()
                        .assertThat().statusCode(200).extract().response().asString();

        JsonPath executionResponse = new JsonPath(response);
        String executionTicketId = executionResponse.getString("id");
        //Update the test execution to test plan
        String updateTestPlanPayload = String.format(graphQLPayload, executionTicketId);
                given().header("Content-Type", "application/json")
                        .header("Authorization", "Bearer " + apiToken)
                        .body(updateTestPlanPayload)
                        .post(graphQLEndpoint).then().log().all().assertThat().statusCode(200);
    }

    public static String convertCucumberReportToString() {
        String filePath = System.getProperty("user.dir") + "/target/cucumber.json";
        String cucumberReportAsString = "";
        try {
            // Read the content of the JSON file into a string
            cucumberReportAsString = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cucumberReportAsString;
    }
}