package com.wamly.testing.automationcore.utils;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.FrameworkException;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class DecryptUtil {

    public static String encrypt(String plainText, String secretKey) {
        byte[] plainTextByte = plainText.getBytes();
        byte[] encryptedByte;
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        try {
            Cipher cipher = Cipher.getInstance(Constants.CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, convertStringToSecretKey(secretKey));
            encryptedByte = cipher.doFinal(plainTextByte);
            return encoder.encodeToString(encryptedByte);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException |
                 InvalidKeyException e) {
            throw new FrameworkException("Encryption failed", e);
        }
    }

    public static String decrypt(String encryptedText, String secretKey) {
        java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
        byte[] encryptedTextByte = decoder.decode(encryptedText);
        try {
            Cipher cipher = Cipher.getInstance(Constants.CIPHER_ALGORITHM); //SunJCE provider AES algorithm, mode(optional) and padding schema(optional)
            cipher.init(Cipher.DECRYPT_MODE, convertStringToSecretKey(secretKey));
            byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
            return new String(decryptedByte);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException |
                 InvalidKeyException e) {
            throw new FrameworkException("Encryption failed", e);
        }
    }

    private static SecretKey convertStringToSecretKey(String secretKeyString) {
        byte[] keyBytes = java.util.Base64.getDecoder().decode(secretKeyString);
        return new SecretKeySpec(keyBytes, Constants.CIPHER_ALGORITHM);
    }
}
