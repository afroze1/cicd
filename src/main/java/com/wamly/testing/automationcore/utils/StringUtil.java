package com.wamly.testing.automationcore.utils;

public class StringUtil {

    private StringUtil() {
    }

    public static boolean isStartsWith(String actualString, String startWithString) {

        return actualString.startsWith(startWithString);
    }

    public static String append(String string, String... stringsToAppend) {

        StringBuilder stringBuilder = new StringBuilder(string);

        for (String str : stringsToAppend) {
            stringBuilder.append(str);
        }

        return stringBuilder.toString();
    }
}
