package com.wamly.testing.automationcore.utils;

import java.util.HashMap;
import java.util.Map;

public class CommonOps {

    private CommonOps() {

    }

    public static Map<String, String> addStringArrayToMap(String[] keys, String[] values) {
        if (keys.length != values.length) {
            throw new IndexOutOfBoundsException("The keys and values arrays should have same size");
        }
        Map<String, String> toMap = new HashMap<>();
        int itr = 0;
        for (String key : keys) {
            toMap.put(key.trim(), values[itr].trim());
            itr++;
        }
        return toMap;

    }
}
