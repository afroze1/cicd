package com.wamly.testing.automationcore.utils;

import com.wamly.testing.automationcore.Constants;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import java.io.File;
import java.util.Map;

public class EmailUtil {

    private EmailUtil() {

    }

    public static void sendHTMLEmail(String html) throws EmailException {
        Map<String, String> toEmailMap;
        HtmlEmail email = new HtmlEmail();
        email.setHostName(Constants.EMAIL_HOST);
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(Constants.EMAIL_AUTH_NAME, Constants.EMAIL_AUTH_PWD));
        email.setSSLOnConnect(true);
        email.setSSLCheckServerIdentity(true);

        String[] toMailIdSet = Constants.EMAIL_TO_EMAIL.split(",");
        String[] toMailNameSet = Constants.EMAIL_TO_NAME.split(",");
        try {
            toEmailMap = CommonOps.addStringArrayToMap(toMailIdSet, toMailNameSet);
            for (Map.Entry<String, String> idSet : toEmailMap.entrySet()) {
                email.addTo(idSet.getKey(), idSet.getValue());
            }

            email.setFrom(Constants.EMAIL_FROM_EMAIL, Constants.EMAIL_FROM_NAME);
            email.setSubject(Constants.EMAIL_SUBJECT);

            // set the html message
            email.setHtmlMsg(html);

            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages");
            if (FileOps.isFileExists(Constants.CUCUMBER_REPORT_PATH))
                email.attach(new File(Constants.CUCUMBER_REPORT_PATH));

            // send the email
            email.send();
        } catch (EmailException e) {
            TestLog.log().error("Failed to send email report", e);
        } catch (IndexOutOfBoundsException e) {
            TestLog.log().error("Failed to send email report. Please check the email.to.email and mail.to.name added correctly", e);
        }

    }
}
