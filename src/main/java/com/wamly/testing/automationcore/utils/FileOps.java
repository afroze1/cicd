package com.wamly.testing.automationcore.utils;

import com.wamly.testing.automationcore.FrameworkException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileOps {

    private FileOps() {
    }

    public static boolean isFileExists(String filePath){
        File file = new File(filePath);
        return file.exists();
    }

    public static boolean copyFile(File file, String imagePath) {
        try {
            FileUtils.copyFile(file, new File(imagePath));
            return true;
        } catch (IOException e) {
            throw new FrameworkException(e);
        }
    }

    public static void createDir(String outputDir) {
        File dir = new File(outputDir);
        if (!dir.exists()) dir.mkdirs();
    }

    public static boolean saveStringIntoFile(String content, String outputFilePath) {
        File file = new File(outputFilePath);
        try (
                BufferedWriter bw = new BufferedWriter(new FileWriter(file))
        ) {
            bw.write(content);
            return true;
        } catch (IOException e) {
            throw new FrameworkException(e);
        }
    }

    public static boolean saveMedia(String media, String outputFilePath) {
        try (FileOutputStream stream = new FileOutputStream(outputFilePath)) {
            stream.write(Base64.decodeBase64(media));
            return true;
        } catch (IOException e) {
            throw new FrameworkException(e);
        }
    }

    public static String readFileAsString(String filePath){
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))){
            return IOUtils.toString(br);
        }catch (IOException e) {
            throw new FrameworkException(e);
        }
    }

    public static String getFileNameFromURI(URI uri) {
        String path = uri.getPath();
        if (path == null) {
            return null; // URI doesn't have a valid path
        }

        // Get the file name from the last part of the path
        File file = new File(path);
        return file.getName();
    }

    // Function to list all JSON files in a directory
    public static List<File> getFilesFromDir(String directoryPath, String extension) {
        File directory = new File(directoryPath);

        if (!directory.exists() || !directory.isDirectory()) {
            throw new IllegalArgumentException("The specified directory does not exist or is not a directory.");
        }

        try (Stream<File> fileStream = Stream.of(directory.listFiles())) {
            return fileStream
                    .filter(File::isFile)
                    .filter(file -> file.getName().endsWith(extension))
                    .collect(Collectors.toList());
        }
    }
}
