package com.wamly.testing.automationcore.utils;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.wamly.testing.automationcore.FrameworkException;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CSV {

    private CSV(){}

    public static List<Map<String, String>> read(File file) {
        List<Map<String, String>> response = new LinkedList<>();
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        MappingIterator<Map<String, String>> iterator;
        try {
            iterator = mapper.readerFor(Map.class)
                    .with(schema)
                    .readValues(file);
        } catch (IOException e) {
            throw new FrameworkException(String.format("Failed to read the CSV file %s",file.getPath()),e);
        }
        while (iterator.hasNext()) {
            response.add(iterator.next());
        }
        return response;
    }
}
