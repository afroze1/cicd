package com.wamly.testing.automationcore.restassured;


import com.fasterxml.jackson.databind.JsonNode;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.utils.JSON;
import com.wamly.testing.automationcore.utils.StringUtil;
import com.wamly.testing.automationcore.utils.TestLog;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

public final class ResponseUtils {

    private ResponseUtils() {
    }

    public static boolean isJSONArray(ResponseOptions<Response> response) {
        return response.getBody().jsonPath().prettyPrint().startsWith("[");
    }

    public static String readValueFromJsonString(String json, String path) {

        JsonNode jsonNode = JSON.parseToJSONNode(json);
        if (!StringUtil.isStartsWith(path, "/") && !path.isEmpty()) {

            path = StringUtil.append("/", path);
        }
        try {
            return jsonNode.at(path).toString();
        } catch (NullPointerException e) {
            TestLog.log().info(String.format("Unable to read node in the path '%s' from the JSON string. Check if given path exists in the JSON string", path), e);
            throw new FrameworkException(String.format("Unable to read node in the path '%s' from the JSON string. Check if given path exists in the JSON string", path), e);
        }
    }

    public static JsonNode readValueAsJsonNodeFromJsonString(String json, String path) {

        JsonNode jsonNode = JSON.parseToJSONNode(json);
        if (!StringUtil.isStartsWith(path, "/") && !path.isEmpty()) {

            path = StringUtil.append("/", path);
        }
        return jsonNode.at(path);
    }

}