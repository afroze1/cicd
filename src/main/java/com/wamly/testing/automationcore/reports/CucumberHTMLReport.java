package com.wamly.testing.automationcore.reports;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.Params;
import com.wamly.testing.automationcore.utils.FileOps;
import com.wamly.testing.automationcore.utils.JSON;
import com.wamly.testing.automationcore.utils.TestLog;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

import java.io.*;
import java.util.*;

public class CucumberHTMLReport {

    private CucumberHTMLReport() {
    }

    public static void generateReport() {
        File reportOutputDirectory = new File("target");
        List<String> jsonFiles = new ArrayList<>();
        mergeJSONReports();
        jsonFiles.add("target/cucumber.json"); //TODO: convert the path as a property
        Configuration configuration = new Configuration(reportOutputDirectory, Constants.PROJECT_NAME);
        configuration.addClassifications("Type", Params.getAppType());
        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
    }

    private static void mergeJSONReports(){
        Map<String, JsonNode> mergedMap = new HashMap<>();

        // Specify the path to the input json and output merged JSON file
        String inputDirectoryPath = "target/cucumber-json/";
        String outputPath = "target/cucumber.json";

        try {
            List<File> jsonFiles = FileOps.getFilesFromDir(inputDirectoryPath, ".json");
            if (jsonFiles.isEmpty()) {
                TestLog.log().error("No cucumber JSON files found in the directory target/cucumber-json/.");
                return;
            }

            for (File jsonFile : jsonFiles) {
                JsonNode jsonData = JSON.parseToJSONNode(jsonFile);
                mergeJsonReportContents(mergedMap, jsonData);
            }
            JsonNode mergedJson = JSON.convertToJsonArray(mergedMap);

            // Write the merged JSON to the output file
            JSON.writeJsonToFile(mergedJson, outputPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void mergeJsonReportContents(Map<String, JsonNode> mergedMap, JsonNode... nodes) {
        for (JsonNode node : nodes) {
            Iterator<JsonNode> elements = node.elements();
            while (elements.hasNext()) {
                JsonNode element = elements.next();
                String id = element.get("id").asText();

                if (mergedMap.containsKey(id)) {
                    JsonNode mergedElement = mergedMap.get(id);
                    ArrayNode mergedElements = (ArrayNode) mergedElement.get("elements");
                    mergedElements.addAll((ArrayNode) element.get("elements"));
                } else {
                    mergedMap.put(id, element);
                }
            }
        }
    }

}
