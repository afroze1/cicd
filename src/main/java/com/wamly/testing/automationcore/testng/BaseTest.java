package com.wamly.testing.automationcore.testng;

import com.wamly.testing.automationcore.Constants;
import com.wamly.testing.automationcore.FrameworkException;
import com.wamly.testing.automationcore.Params;
import com.wamly.testing.automationcore.Settings;
import com.wamly.testing.automationcore.reports.SoftAssertExt;
import com.wamly.testing.automationcore.selenium.ParentDriver;
import com.wamly.testing.automationcore.selenium.SeleniumParams;
import com.wamly.testing.automationcore.selenium.mobile.AppiumParams;
import com.wamly.testing.automationcore.utils.Config;
import com.wamly.testing.automationcore.utils.TestLog;
import org.testng.annotations.*;

public class BaseTest extends Settings {

    protected ParentDriver driver;
    protected SoftAssertExt softAssert;

    /**
     * To perform the pre-requisites before starting the test suite execution
     *
     * @param appType       : type of the app under test(optional). Will proceed with default in case not passed from suite xml
     * @param platformName: device platform. Will proceed with default in case not passed from suite xml
     */
    @Parameters({"appType", "platformName"})
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(@Optional String appType, @Optional String platformName) {
        Config.initSystemTestConfig();
        Params.setAppType(appType);
        switch (Params.getAppType().toLowerCase()) {
            case Constants.TYPE_MOBILE_WEB:
            case Constants.TYPE_MOBILE_NATIVE:
                String[] portNumbers = Constants.APPIUM_PORT.split(":");
                AppiumParams.setPlatformName(platformName);
                for (String portNumberStr : portNumbers) {
                    int portNumber = Integer.parseInt(portNumberStr);
                    startAppiumServer(portNumber);
                }
                break;
            case Constants.TYPE_WEB:
            case Constants.TYPE_API:
            case Constants.TYPE_INFRA:
                //do nothing
                break;
            default:
                throw new FrameworkException(String.format("App type not supported by framework. App Type: %s", Params.getAppType()));

        }

    }

    /**
     * To initialize the test parameters
     *
     * @param deviceName : device name for mobile test
     * @param browser:   browser name for web test
     */
    @Parameters({"deviceName", "browser"})
    @BeforeTest(alwaysRun = true)
    public void beforeTest(@Optional String deviceName, @Optional String browser) {

        switch (Params.getAppType().toLowerCase()) {
            case Constants.TYPE_MOBILE_WEB:
            case Constants.TYPE_MOBILE_NATIVE:
                SeleniumParams.setBrowser(browser);
                if (!Constants.TEST_APPIUM_TRIGGER.equalsIgnoreCase("device_farm")) {
                    AppiumParams.initDeviceParams(deviceName);
                    deviceName = AppiumParams.getDeviceName();
                }
                else deviceName = System.getenv("DEVICEFARM_DEVICE_NAME");
                TestLog.log().info("Device Name : {}", deviceName);
                break;
            case Constants.TYPE_WEB:
                SeleniumParams.setBrowser(browser);
                break;
            case Constants.TYPE_API:
            case Constants.TYPE_INFRA:
                //do nothing
                break;
            default:
                throw new FrameworkException(String.format("Unsupported App Type by Framework : %s",
                        Params.getAppType().toLowerCase()));

        }
    }

    /**
     * To initialize the drivers and  before each method.
     */
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {

        switch (Params.getAppType().toLowerCase()) {
            case Constants.TYPE_MOBILE_WEB:
            case Constants.TYPE_MOBILE_NATIVE:
            case Constants.TYPE_WEB:
                driver = createNewDriverInstance(Params.getAppType().toLowerCase()
                        , AppiumParams.getPlatformName(), SeleniumParams.getBrowser());
                driver.open();
                break;
            case Constants.TYPE_API:
            case Constants.TYPE_INFRA:
                //do nothing
                break;
            default:
                throw new FrameworkException(String.format("Unsupported Test Type by Framework : %s",
                        Params.getAppType().toLowerCase()));

        }
        softAssert = new SoftAssertExt();
    }

    /**
     * Tear down the browser
     */
    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        switch (Params.getAppType().toLowerCase()) {
            case Constants.TYPE_MOBILE_WEB:
            case Constants.TYPE_MOBILE_NATIVE:
            case Constants.TYPE_WEB:
//                driver.close();
                break;
            case Constants.TYPE_API:
            case Constants.TYPE_INFRA:
                //do nothing
                break;
            default:
                throw new FrameworkException(String.format("Unsupported App Type by Framework : %s",
                        Params.getAppType().toLowerCase()));
        }
    }

    /**
     * Tear down the appium server and unload the thread local variables
     */
    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        stopAppiumServer();
        this.unload();
        SeleniumParams.unload();
        AppiumParams.unload();
    }


    /**
     * @return the driver instance
     */
    public ParentDriver getDriver() {
        return driver;
    }

    /**
     * @return the SoftAssertObject
     */
    public SoftAssertExt getSoftAssert() {
        return softAssert;
    }

}
