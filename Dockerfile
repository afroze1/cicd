# Use an official OpenJDK image as a parent image
FROM openjdk:8-jdk-slim

# Install Maven
RUN apt-get update && apt-get install -y maven

# Set the working directory in the container
#WORKDIR /usr/src/app
WORKDIR /tmp

# Copy the Maven project files
COPY pom.xml .

# Download and install project dependencies
RUN mvn -B dependency:resolve dependency:resolve-plugins

# Copy the application source code
#COPY src/ /usr/src/app/src/
COPY . .

# Set environment variables
ENV project_name="Wamly Test Automation Framework"
ENV test_base_url=https://ui.uatwamly.co.za/
ENV test_environment=PROD
ENV test_device_config=devices.xml
ENV test_security_key=Wamly@QbPoV12345!
ENV test_username=+MbXizywzaPpLNktZ0zxtSz9LANt35QWqHCrQnspjSQ=
ENV test_password=VgHaHa3JaTT0+uSA8S7YyA==
ENV test_clientId=X7KOmNad5mSLnjn/urnnsac2r9Mgtk9dD3vM3YYJGqw=
ENV test_secretHash=7z0g7eSuOAkjm61xTgpwA7rR1lsJSYQSidDSkXhXaGDKBZulWppkci8zXyyGszTn
ENV test_browser=chrome
ENV test_headless=true
ENV test_browser_log=false
ENV test_mode=STANDALONE
ENV test_appium_trigger=local
ENV test_locator_path=/com/wamly/testing/automationcore/locators
ENV test_testdata_path=/com/wamly/testing/automationcore/testdata
ENV test_payload_path=/com/wamly/testing/automationcore/payload
ENV test_extent_path=target/extent-report.html
ENV test_cucumber_path=target/cucumber-html/cucumber.html
ENV default_app_type=web
ENV default_platform_name=Android
ENV default_appium_device=Pixel_3
ENV test_candidate_username=candidatejrny@mailsac.com
ENV test_mailsac_baseUrl=https://mailsac.com/api/addresses/candidatejrny@mailsac.com/
ENV test_mailsac_endUrl=messages?_mailsacKey=k_z3Uy5fRoiWcTgDC4DZWsGdPXGBQlStlaqw5f51
ENV xray_baseApi=https://xray.cloud.getxray.app
ENV xray_xrayAuthenticationApi=/api/v1/authenticate
ENV xray_createExecution=/api/v1/import/execution/cucumber?projectKey=WM
ENV xray_graphQLApi=/api/v2/graphql
ENV xray_client_id=QUFCRkZDOTQ0REVBNEU3MTlDMkRDODFEODVCQUI4MEM=
ENV xray_client_secret=MTJjMWVhMDgzNzc0YzM0Y2ZhYzM1MjJjMzhkOGY0MzA0MmM2NmQyYmMwZGRhNmI3ODFjMjhkN2ZlMmNlOTg3OQ==
ENV xray_autheticateBody="{\"client_id\": \"%s\",\"client_secret\": \"%s\"}"
ENV xray_graphqlBody="{\"query\": \"mutation { addTestExecutionsToTestPlan(issueId: \\\"10020\\\", testExecIssueIds: [\\\"%s\\\"]) { addedTestExecutions warning } }\"}"


COPY  . .


# Build the application
#RUN mvn clean test "-Dtestng.suitexml=wamly_temp.xml"
#RUN mvn clean package "-Dtestng.suitexml=wamly_temp.xml"
CMD [ "mvn clean test "-Dtestng.suitexml=wamly_temp.xml"" ]



#COPY /target/automationcore-1.0.2-SNAPSHOT.jar /tmp/

# Expose any necessary ports
#EXPOSE 8080

# Define the command to run your application
#CMD ["sh" , "-c" "java -jar", "/automationcore-1.0.2-SNAPSHOT.jar"]
#target\automationcore-1.0.2-SNAPSHOT.jar